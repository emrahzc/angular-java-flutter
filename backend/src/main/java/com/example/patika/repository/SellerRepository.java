package com.example.patika.repository;

import com.example.patika.model.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SellerRepository extends JpaRepository<Seller,Long> {

    List<Seller> findByProducts_id(@Param("productId") Long productId);
}

