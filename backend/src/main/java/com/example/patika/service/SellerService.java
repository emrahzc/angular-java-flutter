package com.example.patika.service;

import com.example.patika.dto.SellerDto;
import com.example.patika.mapper.SellerMapper;
import com.example.patika.model.Product;
import com.example.patika.model.Seller;
import com.example.patika.repository.ProductRepository;
import com.example.patika.repository.SellerRepository;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class SellerService {

    private final ProductRepository productRepository;
    private final SellerRepository sellerRepository;


    public List<SellerDto> getSellers() {
        return SellerMapper.INSTANCE.toDtoList(sellerRepository.findAll());
    }

    public SellerDto createSeller(SellerDto sellerDto) {
        return SellerMapper.INSTANCE.toDto(sellerRepository.save(SellerMapper.INSTANCE.toModel(sellerDto)));

    }


}
