START TRANSACTION;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+02:00";

CREATE TABLE `category` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `category` (`name`) VALUES
('smartphone'),
('tablet');

CREATE TABLE `product` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `fullname` varchar(1000) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  CONSTRAINT `fk_product_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `product` (`name`, `fullname`, `category_id`) VALUES
('Samsung Galaxy M21', 'Samsung Galaxy M21 SM-M215F Çift SIM, Akıllı Telefon, 64 GB, Siyah', 1),
('Apple iPhone 12 Pro Max', 'Apple iPhone 12 Pro Max Akıllı Telefon, 256 GB, Pacific Blue, Kulaklık ve Adaptör Hariç (Apple Türkiye Garantili) ', 2);

CREATE TABLE `seller` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `seller` (`name`) VALUES
('deveyuku'),
('incehesap');

CREATE TABLE `marketplace` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `homepage` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `marketplace` (`name`, `homepage`) VALUES
('akakce', 'https://www.akakce.com'),
('amazon', 'https://www.amazon.com.tr'),
('hepsiburada', 'https://www.hepsiburada.com.tr');


CREATE TABLE `price` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `job_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `price` double NOT NULL,
  `marketplace_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  CONSTRAINT `fk_price_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `fk_price_seller` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`),
  CONSTRAINT `fk_price_marketplace` FOREIGN KEY (`marketplace_id`) REFERENCES `marketplace` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `product_list_page` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `path` varchar(200) NOT NULL,
  `marketplace_id` int(11) NOT NULL,
  `category_id` int(11) ,
  CONSTRAINT `fk_product_list_page_marketplace` FOREIGN KEY (`marketplace_id`) REFERENCES `marketplace` (`id`),
  CONSTRAINT `fk_product_list_page_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `product_list_page` (`marketplace_id`, `path`, `category_id`) VALUES
(1, '/cep-telefonu.html', 1),
(1, '/tablet.html', 2),
(2, '/b/?node=13709907031', 1);

CREATE TABLE `product_page` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `path` varchar(200) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `product_id` int(11) ,
  CONSTRAINT `fk_product_page_parent` FOREIGN KEY (`parent_id`) REFERENCES `product_list_page` (`id`),
  CONSTRAINT `fk_product_page_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `product_page` (`parent_id`, `path`, `product_id`) VALUES
(1, '/s?k=iphone', 2),
(1, '/cep-telefonu/en-ucuz-iphone-11-64-gb-fiyati,424361143.html', 1),
(3, '/samsung/android-telefonlar-c-60005201', 1),
(3, '/samsung/android-telefonlar-c-60005201', 1),
(3, '/samsung/android-telefonlar-c-60005201', 1),
(3, '/samsung/android-telefonlar-c-60005201', 1);


CREATE TABLE `schedule` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `created_by` varchar(100) NOT NULL,
  `cron` varchar(100) NOT NULL,
  `marketplace_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `type`  ENUM('PRODUCT','LIST')  not null default 'PRODUCT',
  CONSTRAINT `fk_schedule_marketplace` FOREIGN KEY (`marketplace_id`) REFERENCES `marketplace` (`id`),
  CONSTRAINT `fk_schedule_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `job` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `triggered_by` varchar(100) NOT NULL,
  `execution_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `schedule_id` int(11) NOT NULL,
  `status`  ENUM('REGISTERED', 'STARTED', 'FINISHED', 'FAILED')  not null default 'REGISTERED',
  `last_product_page_id` int(11),
  CONSTRAINT `fk_job_schedule` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `selector_group` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` ENUM('list','product') NOT NULL,
  `marketplace_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `selector` (
  `id` int(11)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `selector_type` ENUM('price','seller', 'url', 'image', 'next', 'more') NOT NULL,
  `value` TEXT NOT NULL, 
    CONSTRAINT `fk_selector_group` FOREIGN KEY (`group_id`) REFERENCES `selector_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

COMMIT;
