import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import 'react-bootstrap-typeahead/css/Typeahead.css';

import MarketplacesList from "./components/marketplace/marketplace-list.component";
import AddMarketplace from "./components/marketplace/add-marketplace.component";
import Marketplace from "./components/marketplace/marketplace.component";
import CategoryList from "./components/category/category-list.component";
import ProductPage from "./components/product-page/product-page.component";
import ProductPageList from "./components/product-page/product-page-list.component";
import Schedule from "./components/schedule/schedule.component";
import AddSchedule from "./components/schedule/add-schedule.component";
import SchedulesList from "./components/schedule/schedule-list.component";
import AddCategory from "./components/category/add-category.component";
import Category from "./components/category/category.component";
import JobList from "./components/job/job-list.component";
import AddJob from "./components/job/add-job.component";
import Job from "./components/job/job.component";
import AddProductPage from "./components/product-page/add-product-page.component";
import Login from "./components/login/login";
import { changeToken } from "./http-common";
import ProductListPageList from "./components/product-list-page/product-list-page-list.component";
import AddProductListPage from "./components/product-list-page/add-product-list-page.component";
import ProductListPage from "./components/product-list-page/product-list-page.component";


class App extends Component {
  constructor(props){
    super(props);
    this.state = {token: localStorage.getItem("token")};
  }

  render() {

      if(!this.state.token) {
        return <Login setToken={(token) => {this.setState({token}); changeToken(token);  }} />
      }
    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/marketplaces"} className="navbar-brand" >
            fiyat-la
          </Link>
          <div className="navbar-nav mr-auto">
          <li className="nav-item">
              <Link to={"/marketplaces"} className="nav-link">
                Marketplaces
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/categories"} className="nav-link">
                Categories
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/product-list-pages"} className="nav-link">
                Product List Pages
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/product-pages"} className="nav-link">
                Product Pages
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/schedules"} className="nav-link">
                Schedules
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/jobs"} className="nav-link">
                Jobs
              </Link>
            </li>
          </div>
          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <a className="nav-link" href='fiyatla.zip' download >Download Capture Extension</a>
            </li>
            <li className="nav-item">
              <button className="btn btn-link"   onClick={() => changeToken("")}>Logout</button>
            </li>
          </div>

        </nav>

        <div className="container mt-8">
          <Switch>
            <Route exact path={["/", "/marketplaces"]} component={MarketplacesList} />
            <Route exact path="/marketplace/add" component={AddMarketplace} />
            <Route path="/marketplace/:id" component={Marketplace} />
            <Route exact path={["/", "/categories"]} component={CategoryList} />
            <Route exact path="/category/add" component={AddCategory} />
            <Route path="/category/:id" component={Category} />
            <Route exact path={["/jobs"]} component={JobList} />
            <Route exact path="/job/add" component={AddJob} />
            <Route path="/job/:id" component={Job} />
            <Route exact path={["/product-pages"]} component={ProductPageList} />
            <Route exact path="/product-page/add" component={AddProductPage} />
            <Route path="/product-page/:id" component={ProductPage} />
            <Route exact path={["/product-list-pages"]} component={ProductListPageList} />
            <Route exact path="/product-list-page/add" component={AddProductListPage} />
            <Route path="/product-list-page/:id" component={ProductListPage} />
            <Route exact path={["/schedules"]} component={SchedulesList} />
            <Route exact path="/schedule/add" component={AddSchedule} />
            <Route path="/schedule/:id" component={Schedule} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
