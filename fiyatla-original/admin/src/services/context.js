import React from 'react';

export const DataContext = React.createContext({
    token: '',
    isLoggedIn: false
});