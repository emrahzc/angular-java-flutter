import http from "../http-common";

class ProductPageService {
  getAll() {
    return http.get("/productPage");
  }

  get(id) {
    return http.get(`/productPage/${id}`);
  }

  create(data) {
    return http.post("/productPage", data);
  }

  update(id, data) {
    return http.put(`/productPage/${id}`, data);
  }

  delete(id) {
    return http.delete(`/productPage/${id}`);
  }

  deleteAll() {
    return http.delete(`/productPage`);
  }

  searchByName(name) {
    return http.get(`/productPage/search/${name}`);
  }
}

export default new ProductPageService();