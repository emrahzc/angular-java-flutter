import http from "../http-common";

class JobService {
  getAll() {
    return http.get("/job");
  }

  get(id) {
    return http.get(`/job/${id}`);
  }

  create(data) {
    console.log("jobservice create", data);
    return http.post("/job", data);
  }

  update(id, data) {
    return http.put(`/job/${id}`, data);
  }

  delete(id) {
    return http.delete(`/job/${id}`);
  }

  deleteAll() {
    return http.delete(`/job`);
  }

  searchByName(name) {
    return http.get(`/job/search/${name}`);
  }
}

export default new JobService();