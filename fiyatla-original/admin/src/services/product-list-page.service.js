import http from "../http-common";

class ProductListPageService {
  getAll() {
    return http.get("/productListPage");
  }

  get(id) {
    return http.get(`/productListPage/${id}`);
  }

  create(data) {
    return http.post("/productListPage", data);
  }

  update(id, data) {
    return http.put(`/productListPage/${id}`, data);
  }

  delete(id) {
    return http.delete(`/productListPage/${id}`);
  }

  deleteAll() {
    return http.delete(`/productListPage`);
  }

  search(data) {
    return http.post(`/productListPage/search/`, data);
  }
}

export default new ProductListPageService();