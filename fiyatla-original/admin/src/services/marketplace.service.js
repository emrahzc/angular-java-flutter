import http from "../http-common";

class MarketplaceService {
  getAll() {
    return http.get("/marketplace");
  }

  get(id) {
    return http.get(`/marketplace/${id}`);
  }

  create(data) {
    return http.post("/marketplace", data);
  }

  update(id, data) {
    return http.put(`/marketplace/${id}`, data);
  }

  delete(id) {
    return http.delete(`/marketplace/${id}`);
  }

  deleteAll() {
    return http.delete(`/marketplace`);
  }

  findByTitle(title) {
    return http.get(`/marketplace?title=${title}`);
  }
}

export default new MarketplaceService();