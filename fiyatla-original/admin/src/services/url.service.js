import http from "../http-common";

class UrlService {
  getAll() {
    return http.get("/url");
  }

  get(id) {
    return http.get(`/url/${id}`);
  }

  getByMarketplace(id) {
    return http.get(`/url/marketplace/${id}`);
  }

  create(data) {
    return http.post("/url", data);
  }

  update(id, data) {
    return http.put(`/url/${id}`, data);
  }

  delete(id) {
    return http.delete(`/url/${id}`);
  }

  deleteAll() {
    return http.delete(`/url`);
  }

  findByTitle(title) {
    return http.get(`/url?title=${title}`);
  }
}

export default new UrlService();