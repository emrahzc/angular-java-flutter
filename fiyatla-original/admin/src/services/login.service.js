import axios from "axios";

class LoginService {
  login(data) {
    return axios.create({
      baseURL: "http://"+window.location.host+":3000/api",
      headers: {
        "Content-type": "application/json"            
      }
    }).post("/auth/login", data);
  }
}

export default new LoginService();