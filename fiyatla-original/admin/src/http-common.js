import axios from "axios";
let tkn = localStorage.getItem("token");
const headers = {};
if(tkn){
  headers["Authorization"] = 'Bearer '+ tkn;
}
let httpCommon = axios.create({
  baseURL: "http://"+window.location.host+":3000/api",
  headers
});

const changeToken = (token) => {
  localStorage.setItem("token", token);
  window.location.reload(); 
};


export default httpCommon;
export {changeToken}