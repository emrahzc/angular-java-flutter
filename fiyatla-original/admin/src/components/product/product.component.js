import React, { Component } from "react";
import ProductService from "../../services/product.service";

export default class Product extends Component {
  state = {
    currentProduct: {},
    message: ""
  };
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeHomePage = this.onChangeHomePage.bind(this);
    this.getProduct = this.getProduct.bind(this);
    this.updatePublished = this.updatePublished.bind(this);
    this.updateProduct = this.updateProduct.bind(this);
    this.deleteProduct = this.deleteProduct.bind(this);
    console.log("mymarket:", this.props.match.params.id);
  
  }

  componentDidMount() {
    this.getProduct(this.props.match.params.id);
  }

  onChangeTitle(e) {
    const name = e.target.value;

    this.setState(function(prevState) {
      return {
        currentProduct: {
          ...prevState.currentProduct,
          name: name
        }
      };
    });
  }

  onChangeHomePage(e) {
    const homepage = e.target.value;

    this.setState(prevState => ({
      currentProduct: {
        ...prevState.currentProduct,
        homepage: homepage
      }
    }));
  }

  getProduct(id) {
    ProductService.get(id)
      .then(response => {
        this.setState({
          currentProduct: response.data.products[0]
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePublished(status) {
    var data = {
      id: this.state.currentProduct.id,
      name: this.state.currentProduct.name,
      homepage: this.state.currentProduct.homepage,
      published: status
    };

    ProductService.update(this.state.currentProduct.id, data)
      .then(response => {
        this.setState(prevState => ({
          currentProduct: {
            ...prevState.currentProduct,
            published: status
          }
        }));
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateProduct() {
    ProductService.update(
      this.state.currentProduct.id,
      this.state.currentProduct
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The product was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteProduct() {    
    ProductService.delete(this.state.currentProduct.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/products')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentProduct } = this.state;
    console.log(currentProduct);
    return (
        currentProduct ? (
         <div className="row">

    <div className="edit-form col-md-6">
          <h4>Product</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Title</label>
              <input
                type="text"
                className="form-control"
                id="name"
                defaultValue={currentProduct.name }
                onChange={this.onChangeTitle}
              />
            </div>
            <div className="form-group">
              <label htmlFor="homepage">HomePage</label>
              <input
                type="text"
                className="form-control"
                id="homepage"
                defaultValue={currentProduct.homepage}
                onChange={this.onChangeHomePage}
              />
            </div>

            <div className="form-group">
              <label>
                <strong>Status:</strong>
              </label>
              {currentProduct.published ? "Published" : "Pending"}
            </div>
          </form>

          {currentProduct.published ? (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(false)}
            >
              UnPublish
            </button>
          ) : (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(true)}
            >
              Publish
            </button>
          )}

          <button
            className="badge badge-danger mr-2"
            onClick={this.deleteProduct}
          >
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={this.updateProduct}
          >
            Update
          </button>
          <p>{this.state.message}</p>
        </div>
 
 
        <div className="edit-form col-md-6">
          <h4>Product</h4>

        </div>
 
         </div>      ) : (
          <div>
            <br />
            <p>Please click on a Product...</p>
          </div>
        )
    );
  }
}
