import React from "react";
import MarketplaceService from "../../services/marketplace.service";
import ProductService from "../../services/product.service";

export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.saveProduct = this.saveProduct.bind(this);
    this.newProduct = this.newProduct.bind(this);

    this.state = {
      id: null,
      path: "",
      submitted: false,
      marketplaces: [],
      marketplaceId: null,
      type: ''
    };
  }
  componentDidMount() {
    MarketplaceService.getAll().then(response => {
        this.setState({
          marketplaces: response.data.marketplaces
        });
        console.log(response);
      })
      .catch(e => {
        console.log(e);
      });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  saveProduct() {
    var data = {
      path: this.state.path,
      type: this.state.type,
      marketplaceId: this.state.marketplaceId
    };
  console.log("sending:", data);
    ProductService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          path: response.data.path,
          type: response.data.type,
          submitted: true
        });
        console.log(response.data);
        this.props.retrieveProducts();
      })
      .catch(e => {
        console.log(e);
      });
  }

  newProduct() {
    this.setState({
      id: null,
      path: "",
      marketplaceId: null,
      type: '',
      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>Product Added!</h4>
            <button className="btn btn-success" onClick={this.newProduct}>
              Add New Product
            </button>
          </div>
        ) : (
          <div>
              <h4>Add Product</h4>
              <div className="form-group">
              <select name="marketplaceId" className="form-control" defaultValue = '' onChange={this.onChange}>
                <option value='' disabled >select marketplace...</option>
                {this.state.marketplaces.map(marketplace =>
                  <option key={marketplace.id} value={marketplace.id}>{marketplace.name}  ({marketplace.homepage})</option>
                  )}
              </select>
            </div>

            <div className="form-group">
              <input
                type="text"
                className="form-control"
                id="path"
                required
                placeholder="enter path after host name..."
                value={this.state.path}
                onChange={this.onChange}
                name="path"
              />
            </div>

            <div className="form-group">
              <select name="type" className="form-control" defaultValue={this.state.type} onChange={this.onChange}>
              <option value='' disabled>select content type...</option>
                <option value="product">product</option>
                <option value="category">category</option>
              </select>
            </div>

            <button onClick={this.saveProduct} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}
