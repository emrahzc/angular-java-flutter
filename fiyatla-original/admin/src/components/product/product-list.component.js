import React, { Component } from "react";
import ProductService from "../../services/product.service";
import { Link } from "react-router-dom";
import AddProduct from "./add-product.component";

export default class ProductsList extends Component {
  constructor(props) {
    super(props);
    this.retrieveProducts = this.retrieveProducts.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveProduct = this.setActiveProduct.bind(this);
    this.removeAllProducts = this.removeAllProducts.bind(this);

    this.state = {
      products: [],
      currentProduct: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveProducts();
  }

  retrieveProducts() {
    const promise = this.props.marketplace ? ProductService.getByMarketplace(this.props.marketplace.id) : ProductService.getAll();
    promise.then(response => {
        this.setState({
          products: response.data.products
        });
      })
      .catch(e => {
        console.error(e);
      });
  }

  refreshList() {
    this.retrieveProducts();
    this.setState({
      currentProduct: null,
      currentIndex: -1
    });
  }

  setActiveProduct(product, index) {
    this.setState({
      currentProduct: product,
      currentIndex: index
    });
  }

  removeAllProducts() {
    ProductService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.error(e);
      });
  }

  render() {
    const { products, currentProduct, currentIndex } = this.state;
    return (
      <div className="list row">
        <div className="col-md-8">
          <h4 style = {{display: 'inline-block'}}>Products List</h4>
          {products.length > 0 && <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllProducts}
          >
            Remove All
          </button> }

          <ul className="list-group">
            {products &&
              products.map((product, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveProduct(product, index)}
                  key={index}
                >
                    {( product.homepage ?? this.props.marketplace?.homepage) + product.path}
                </li>
              ))}
          </ul>

        </div>
        <div className="col-md-8">
          {currentProduct ? (
            <div>
              <h4>Product</h4>
              <div>
                <label>
                  <strong>Path:</strong>
                </label>
                {currentProduct.homepage + currentProduct.path}
              </div>
              <div>
                <label>
                  <strong>Type:</strong>
                </label>{currentProduct.type}
                {}
              </div>

              <Link
                to={"/product/" + currentProduct.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : ""
          }
          <AddProduct retrieveProducts={this.retrieveProducts} />
        </div>
      </div>
    );
  }
}
