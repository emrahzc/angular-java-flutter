import React, { Component } from "react";
import CategoryService from "../../services/category.service";
import { Link } from "react-router-dom";
import AddCategory from "./add-category.component";

export default class CategoryList extends Component {
  constructor(props) {
    super(props);
    this.retrieveCategories = this.retrieveCategories.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveCategory = this.setActiveCategory.bind(this);
    this.removeAllCategories = this.removeAllCategories.bind(this);

    this.state = {
      categories: [],
      currentCategory: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveCategories();
  }

  retrieveCategories() {
    CategoryService.getAll()
      .then(response => {
        this.setState({
          categories: response.data.rows
        });
        console.log(response.categories);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveCategories();
    this.setState({
      currentCategory: null,
      currentIndex: -1
    });
  }

  setActiveCategory(category, index) {
    this.setState({
      currentCategory: category,
      currentIndex: index
    });
  }

  removeAllCategories() {
    CategoryService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { categories, currentCategory, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-8">
        <h4 style = {{display: 'inline-block'}}>Categories</h4>
          {categories?.length > 0 && <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllCategories} >Remove All</button> 
          }
          <ul className="list-group">
            {categories &&
              categories.map((category, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveCategory(category, index)}
                  key={index}
                >
                  {category.name}
                </li>
              ))}
          </ul>
        </div>
        <div className="col-md-8">
          {currentCategory && 
            <div>
              <h4>Category</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>
                {currentCategory.name}
              </div>
              <div>
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {currentCategory.homepage}
              </div>

              <Link
                to={"/category/" + currentCategory.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
            }
            <AddCategory retrieveCategories={this.retrieveCategories} />
         
        </div>
      </div>
    );
  }
}
