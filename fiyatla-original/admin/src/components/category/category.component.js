import React, { Component } from "react";
import CategoryService from "../../services/category.service";
import CategoryList from "./category-list.component";

export default class Category extends Component {
  state = {
    currentCategory: {},
    message: ""
  };
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeHomePage = this.onChangeHomePage.bind(this);
    this.getCategory = this.getCategory.bind(this);
    this.updatePublished = this.updatePublished.bind(this);
    this.updateCategory = this.updateCategory.bind(this);
    this.deleteCategory = this.deleteCategory.bind(this);
    console.log("mymarket:", this.props.match.params.id);
  
  }

  componentDidMount() {
    this.getCategory(this.props.match.params.id);
  }

  onChangeTitle(e) {
    const name = e.target.value;

    this.setState(function(prevState) {
      return {
        currentCategory: {
          ...prevState.currentCategory,
          name: name
        }
      };
    });
  }

  onChangeHomePage(e) {
    const homepage = e.target.value;

    this.setState(prevState => ({
      currentCategory: {
        ...prevState.currentCategory,
        homepage: homepage
      }
    }));
  }

  getCategory(id) {
    CategoryService.get(id)
      .then(response => {
        this.setState({
          currentCategory: response.data.categorys[0]
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePublished(status) {
    var data = {
      id: this.state.currentCategory.id,
      name: this.state.currentCategory.name,
      homepage: this.state.currentCategory.homepage,
      published: status
    };

    CategoryService.update(this.state.currentCategory.id, data)
      .then(response => {
        this.setState(prevState => ({
          currentCategory: {
            ...prevState.currentCategory,
            published: status
          }
        }));
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateCategory() {
    CategoryService.update(
      this.state.currentCategory.id,
      this.state.currentCategory
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The category was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteCategory() {    
    CategoryService.delete(this.state.currentCategory.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/categories')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentCategory } = this.state;
    console.log("render",currentCategory);
    return (
        currentCategory ? (
         <div className="row">

    <div className="edit-form col-md-3" style={{    backgroundColor: '#454b5a24'}}>
          <h4>Category</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Title</label>
              <input
                type="text"
                className="form-control"
                id="name"
                defaultValue={currentCategory.name }
                onChange={this.onChangeTitle}
              />
            </div>
            <div className="form-group">
              <label htmlFor="homepage">HomePage</label>
              <input
                type="text"
                className="form-control"
                id="homepage"
                defaultValue={currentCategory.homepage}
                onChange={this.onChangeHomePage}
              />
            </div>

            <div className="form-group">
              <label>
                <strong>Status:</strong>
              </label>
              {currentCategory.published ? "Published" : "Pending"}
            </div>
          </form>

          {currentCategory.published ? (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(false)}
            >
              UnPublish
            </button>
          ) : (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(true)}
            >
              Publish
            </button>
          )}

          <button
            className="badge badge-danger mr-2"
            onClick={this.deleteCategory}
          >
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={this.updateCategory}
          >
            Update
          </button>
          <p>{this.state.message}</p>
        </div>
 
 
        <div className="col-md-9">
           {currentCategory.id && <CategoryList category = {currentCategory} /> }
        </div>
 
         </div>      ) : ""
    );
  }
}
