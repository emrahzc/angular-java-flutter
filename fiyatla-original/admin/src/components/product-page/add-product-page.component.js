import React from "react";
import ProductPageService from "../../services/product-page.service";

export default class AddProductPage extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.saveProductPage = this.saveProductPage.bind(this);
    this.newProductPage = this.newProductPage.bind(this);

    this.state = {
      id: null,
      path: "",
      submitted: false,
      productPages: [],
      productPageId: null,
      type: ''
    };
  }
  componentDidMount() {
    ProductPageService.getAll().then(response => {
        this.setState({
          productPages: response.data.rows || []
        });
        console.log(response);
      })
      .catch(e => {
        console.log(e);
      });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  saveProductPage() {
    ProductPageService.create( {
      path: this.state.path,
      type: this.state.type,
      productPageId: this.state.productPageId
    })
      .then(response => {
        this.setState({
          id: response.data.id,
          path: response.data.path,
          type: response.data.type,
          submitted: true
        });
        console.log(response.data);
        this.props.retrieveProductPages();
      })
      .catch(e => {
        console.log(e);
      });
  }

  newProductPage() {
    this.setState({
      id: null,
      path: "",
      productPageId: null,
      type: '',
      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>ProductPage Added!</h4>
            <button className="btn btn-success" onClick={this.newProductPage}>
              Add New ProductPage
            </button>
          </div>
        ) : (
          <div>
              <h4>Add ProductPage</h4>
              <div className="form-group">
              <select name="productPageId" className="form-control" defaultValue = '' onChange={this.onChange}>
                <option value='' disabled >select productPage...</option>
                {this.state.productPages.map(productPage =>
                  <option key={productPage.id} value={productPage.id}>{productPage.name}  ({productPage.homepage})</option>
                  )}
              </select>
            </div>

            <div className="form-group">
              <input
                type="text"
                className="form-control"
                id="path"
                required
                placeholder="enter path after host name..."
                value={this.state.path}
                onChange={this.onChange}
                name="path"
              />
            </div>

            <div className="form-group">
              <select name="type" className="form-control" defaultValue={this.state.type} onChange={this.onChange}>
              <option value='' disabled>select content type...</option>
                <option value="product">product</option>
                <option value="category">category</option>
              </select>
            </div>

            <button onClick={this.saveProductPage} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}
