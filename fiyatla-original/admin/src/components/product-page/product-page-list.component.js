import React, { Component } from "react";
import ProductPageService from "../../services/product-page.service";
import { Link } from "react-router-dom";
import AddProductPage from "./add-product-page.component";

export default class ProductPageList extends Component {
  constructor(props) {
    super(props);
    this.retrieveProductPages = this.retrieveProductPages.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveProductPage = this.setActiveProductPage.bind(this);
    this.removeAllProductPages = this.removeAllProductPages.bind(this);

    this.state = {
      productPages: [],
      currentProductPage: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveProductPages();
  }

  retrieveProductPages() {
    ProductPageService.getAll().then(response => {
        this.setState({
          productPages: response.data.rows || []
        });
      })
      .catch(e => {
        console.error(e);
      });
  }

  refreshList() {
    this.retrieveProductPages();
    this.setState({
      currentProductPage: null,
      currentIndex: -1
    });
  }

  setActiveProductPage(productPage, index) {
    this.setState({
      currentProductPage: productPage,
      currentIndex: index
    });
  }

  removeAllProductPages() {
    ProductPageService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.error(e);
      });
  }

  render() {
    const { productPages, currentProductPage, currentIndex } = this.state;
    return (
      <div className="list row">
        <div className="col-md-8">
          <h4 style = {{display: 'inline-block'}}>Product Pages</h4>
          {productPages.length > 0 && <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllProductPages}
          >
            Remove All
          </button> }

          <ul className="list-group">
            {productPages &&
              productPages.map((productPage, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveProductPage(productPage, index)}
                  key={index}
                >
                    {( productPage.homepage ?? this.props.marketplace?.homepage) + productPage.path}
                </li>
              ))}
          </ul>

        </div>
        <div className="col-md-8">
          {currentProductPage ? (
            <div>
              <h4>ProductPage</h4>
              <div>
                <label>
                  <strong>Path:</strong>
                </label>
                {currentProductPage.homepage + currentProductPage.path}
              </div>
              <div>
                <label>
                  <strong>Type:</strong>
                </label>{currentProductPage.type}
                {}
              </div>

              <Link
                to={"/productPage/" + currentProductPage.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : ""
          }
          <AddProductPage retrieveProductPages={this.retrieveProductPages} />
        </div>
      </div>
    );
  }
}
