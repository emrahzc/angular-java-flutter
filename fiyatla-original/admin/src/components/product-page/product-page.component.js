import React, { Component } from "react";
import marketplaceService from "../../services/marketplace.service";
import ProductPageService from "../../services/product-page.service";

export default class ProductPage extends Component {
  state = {
    currentProductPage: {},
    message: "",
    marketplaces: []
  };
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.getProductPage = this.getProductPage.bind(this);
    this.updateProductPage = this.updateProductPage.bind(this);
    this.deleteProductPage = this.deleteProductPage.bind(this);
    console.log("mymarket:", this.props.match.params.id);
  
  }

  componentDidMount() {
    this.getProductPage(this.props.match.params.id);
    marketplaceService.getAll().then(response => {
      this.setState({
        marketplaces: response.data.marketplaces
      });
      console.log(response);
    })
    .catch(e => {
      console.log(e);
    });
  }

  onChange(e) {
    this.setState(prevState => ({
      currentProductPage: {
        ...prevState.currentProductPage,
        [e.target.name]: e.target.value
      }
    }));
  }

  getProductPage(id) {
    ProductPageService.get(id)
      .then(response => {
        this.setState({
          currentProductPage: response.data.productPages[0]
        });
        console.log("response:", response);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateProductPage() {
    ProductPageService.update(
      this.state.currentProductPage.id,
      this.state.currentProductPage
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The productPage was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteProductPage() {    
    ProductPageService.delete(this.state.currentProductPage.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/productPages')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentProductPage } = this.state;
    console.log("currentProductPage", currentProductPage)
    return (
      this.state.currentProductPage ? (
         <div className="row">
        <div className="edit-form col-md-6">
          <h4>ProductPage</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Path</label>
              <input
                type="text"
                className="form-control"
                id="path"
                defaultValue={currentProductPage.path }
                onChange={this.onChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="marketplaceId">Marketplace</label>
              <select name="marketplaceId" className="form-control" defaultValue = {currentProductPage.marketplace_id} onChange={this.onChange}>
                {this.state.marketplaces.map(marketplace =>
                  <option key={marketplace.id} value={marketplace.id}>{marketplace.name}  ({marketplace.homepage})</option>
                  )}
              </select>
            </div>
          </form>

          <button
            className="badge badge-danger mr-2"
            onClick={this.deleteProductPage}>Delete</button>

          <button type="submit" className="badge badge-success" onClick={this.updateProductPage}>Update</button>
          <p>{this.state.message}</p>
        </div>
         </div>      ) : (
          <div>
            <br />
            <p>Please click on an item...</p>
          </div>
        )
    );
  }
}
