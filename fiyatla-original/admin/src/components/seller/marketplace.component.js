import React, { Component } from "react";
import ApiService from "../../services/marketplace.service";

export default class Marketplace extends Component {
  state = {
    currentMarketplace: {},
    message: ""
  };
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeHomePage = this.onChangeHomePage.bind(this);
    this.getMarketplace = this.getMarketplace.bind(this);
    this.updatePublished = this.updatePublished.bind(this);
    this.updateMarketplace = this.updateMarketplace.bind(this);
    this.deleteMarketplace = this.deleteMarketplace.bind(this);
    console.log("mymarket:", this.props.match.params.id);
  
  }

  componentDidMount() {
    this.getMarketplace(this.props.match.params.id);
  }

  onChangeTitle(e) {
    const name = e.target.value;

    this.setState(function(prevState) {
      return {
        currentMarketplace: {
          ...prevState.currentMarketplace,
          name: name
        }
      };
    });
  }

  onChangeHomePage(e) {
    const homepage = e.target.value;

    this.setState(prevState => ({
      currentMarketplace: {
        ...prevState.currentMarketplace,
        homepage: homepage
      }
    }));
  }

  getMarketplace(id) {
    ApiService.get(id)
      .then(response => {
        this.setState({
          currentMarketplace: response.data.marketplaces[0]
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePublished(status) {
    var data = {
      id: this.state.currentMarketplace.id,
      name: this.state.currentMarketplace.name,
      homepage: this.state.currentMarketplace.homepage,
      published: status
    };

    ApiService.update(this.state.currentMarketplace.id, data)
      .then(response => {
        this.setState(prevState => ({
          currentMarketplace: {
            ...prevState.currentMarketplace,
            published: status
          }
        }));
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateMarketplace() {
    ApiService.update(
      this.state.currentMarketplace.id,
      this.state.currentMarketplace
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The marketplace was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteMarketplace() {    
    ApiService.delete(this.state.currentMarketplace.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/marketplaces')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentMarketplace } = this.state;
    console.log(currentMarketplace);
    return (
        currentMarketplace ? (
         <div className="row">

    <div className="edit-form col-md-6">
          <h4>Marketplace</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Title</label>
              <input
                type="text"
                className="form-control"
                id="name"
                defaultValue={currentMarketplace.name }
                onChange={this.onChangeTitle}
              />
            </div>
            <div className="form-group">
              <label htmlFor="homepage">HomePage</label>
              <input
                type="text"
                className="form-control"
                id="homepage"
                defaultValue={currentMarketplace.homepage}
                onChange={this.onChangeHomePage}
              />
            </div>

            <div className="form-group">
              <label>
                <strong>Status:</strong>
              </label>
              {currentMarketplace.published ? "Published" : "Pending"}
            </div>
          </form>

          {currentMarketplace.published ? (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(false)}
            >
              UnPublish
            </button>
          ) : (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(true)}
            >
              Publish
            </button>
          )}

          <button
            className="badge badge-danger mr-2"
            onClick={this.deleteMarketplace}
          >
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={this.updateMarketplace}
          >
            Update
          </button>
          <p>{this.state.message}</p>
        </div>
 
 
        <div className="edit-form col-md-6">
          <h4>Marketplace</h4>

        </div>
 
         </div>      ) : (
          <div>
            <br />
            <p>Please click on a Marketplace...</p>
          </div>
        )
    );
  }
}
