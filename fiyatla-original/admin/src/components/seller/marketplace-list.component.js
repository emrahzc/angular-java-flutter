import React, { Component } from "react";
import ApiService from "../../services/marketplace.service";
import { Link } from "react-router-dom";

export default class MarketplacesList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveMarketplaces = this.retrieveMarketplaces.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveMarketplace = this.setActiveMarketplace.bind(this);
    this.removeAllMarketplaces = this.removeAllMarketplaces.bind(this);
    this.searchTitle = this.searchTitle.bind(this);

    this.state = {
      marketplaces: [],
      currentMarketplace: null,
      currentIndex: -1,
      searchTitle: ""
    };
  }

  componentDidMount() {
    this.retrieveMarketplaces();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle
    });
  }

  retrieveMarketplaces() {
    ApiService.getAll()
      .then(response => {
        this.setState({
          marketplaces: response.data.marketplaces
        });
        console.log(response.marketplaces);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveMarketplaces();
    this.setState({
      currentMarketplace: null,
      currentIndex: -1
    });
  }

  setActiveMarketplace(marketplace, index) {
    this.setState({
      currentMarketplace: marketplace,
      currentIndex: index
    });
  }

  removeAllMarketplaces() {
    ApiService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }

  searchTitle() {
    this.setState({
      currentMarketplace: null,
      currentIndex: -1
    });

    ApiService.findByTitle(this.state.searchTitle)
      .then(response => {
        this.setState({
          marketplaces: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { searchTitle, marketplaces, currentMarketplace, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-8">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search by title"
              value={searchTitle}
              onChange={this.onChangeSearchTitle}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={this.searchTitle}
              >
                Search
              </button>
            </div>
          </div>
        </div>
        <div className="col-md-8">
          <h4>Marketplaces List</h4>

          <ul className="list-group">
            {marketplaces &&
              marketplaces.map((marketplace, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveMarketplace(marketplace, index)}
                  key={index}
                >
                  {marketplace.homepage}
                <Link to={"/marketplace/" + marketplace.id} className="badge badge-warning" style ={{float:"right"}}>
                    {marketplace.name}
                </Link>
                </li>
              ))}
          </ul>

          <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllMarketplaces}
          >
            Remove All
          </button>
        </div>
        <div className="col-md-8">
          {currentMarketplace ? (
            <div>
              <h4>Marketplace</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>
                {currentMarketplace.name}
              </div>
              <div>
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {currentMarketplace.homepage}
              </div>

              <Link
                to={"/marketplace/" + currentMarketplace.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : (
            <div>
              <br />
              <p>click on a Marketplace...</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}
