import React, { Component } from "react";
import JobService from "../../services/job.service";
import JobList from "./job-list.component";

export default class Job extends Component {
  state = {
    currentJob: {},
    message: ""
  };
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeHomePage = this.onChangeHomePage.bind(this);
    this.getJob = this.getJob.bind(this);
    this.updatePublished = this.updatePublished.bind(this);
    this.updateJob = this.updateJob.bind(this);
    this.deleteJob = this.deleteJob.bind(this);
    console.log("mymarket:", this.props.match.params.id);
  
  }

  componentDidMount() {
    this.getJob(this.props.match.params.id);
  }

  onChangeTitle(e) {
    const name = e.target.value;

    this.setState(function(prevState) {
      return {
        currentJob: {
          ...prevState.currentJob,
          name: name
        }
      };
    });
  }

  onChangeHomePage(e) {
    const homepage = e.target.value;

    this.setState(prevState => ({
      currentJob: {
        ...prevState.currentJob,
        homepage: homepage
      }
    }));
  }

  getJob(id) {
    JobService.get(id)
      .then(response => {
        this.setState({
          currentJob: response.data.jobs[0]
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePublished(status) {
    var data = {
      id: this.state.currentJob.id,
      name: this.state.currentJob.name,
      homepage: this.state.currentJob.homepage,
      published: status
    };

    JobService.update(this.state.currentJob.id, data)
      .then(response => {
        this.setState(prevState => ({
          currentJob: {
            ...prevState.currentJob,
            published: status
          }
        }));
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateJob() {
    JobService.update(
      this.state.currentJob.id,
      this.state.currentJob
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The job was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteJob() {    
    JobService.delete(this.state.currentJob.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/jobs')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentJob } = this.state;
    console.log("render",currentJob);
    return (
        currentJob ? (
         <div className="row">

    <div className="edit-form col-md-3" style={{    backgroundColor: '#454b5a24'}}>
          <h4>Job</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Title</label>
              <input
                type="text"
                className="form-control"
                id="name"
                defaultValue={currentJob.name }
                onChange={this.onChangeTitle}
              />
            </div>
            <div className="form-group">
              <label htmlFor="homepage">HomePage</label>
              <input
                type="text"
                className="form-control"
                id="homepage"
                defaultValue={currentJob.homepage}
                onChange={this.onChangeHomePage}
              />
            </div>

            <div className="form-group">
              <label>
                <strong>Status:</strong>
              </label>
              {currentJob.published ? "Published" : "Pending"}
            </div>
          </form>

          {currentJob.published ? (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(false)}
            >
              UnPublish
            </button>
          ) : (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(true)}
            >
              Publish
            </button>
          )}

          <button
            className="badge badge-danger mr-2"
            onClick={this.deleteJob}
          >
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={this.updateJob}
          >
            Update
          </button>
          <p>{this.state.message}</p>
        </div>
 
 
        <div className="col-md-9">
           {currentJob.id && <JobList job = {currentJob} /> }
        </div>
 
         </div>      ) : ""
    );
  }
}
