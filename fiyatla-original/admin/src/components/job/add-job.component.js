import React from "react";
import JobService from "../../services/job.service";

export default class AddJob extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.saveJob = this.saveJob.bind(this);
    this.newJob = this.newJob.bind(this);

    this.state = {
      id: null,
      name: "",
      submitted: false
    };
  }

  onChangeName(e) {
    this.setState({
      name: e.target.value
    });
  }

  saveJob() {
    var data = {
      name: this.state.name,
      homepage: this.state.homepage
    };

    JobService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          name: response.data.name,
          submitted: true
        });
        this.props.retrieveJobs();
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  newJob() {
    this.setState({
      id: null,
      name: "",
      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={this.newJob}>
              Add
            </button>
          </div>
        ) : (
          <div>
            <br />
            <br />
            <h4>new job</h4>
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                placeholder = "enter name..."
                id="name"
                required
                value={this.state.name}
                onChange={this.onChangeName}
                name="name"
              />
            </div>
            <button onClick={this.saveJob} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}
