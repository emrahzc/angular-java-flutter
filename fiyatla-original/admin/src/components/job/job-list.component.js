import React, { Component } from "react";
import JobService from "../../services/job.service";
import { Link } from "react-router-dom";
import jobService from "../../services/job.service";

export default class JobList extends Component {
  constructor(props) {
    super(props);
    this.retrieveJobs = this.retrieveJobs.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveJob = this.setActiveJob.bind(this);
    this.removeAllJobs = this.removeAllJobs.bind(this);
    this.repeatJob = this.repeatJob.bind(this);

    this.state = {
      jobs: [],
      currentJob: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveJobs();
  }

  retrieveJobs() {
    jobService.getAll()
      .then(response => {
        this.setState({
          jobs: response.data.rows
        });
        console.log(response.jobs);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveJobs();
    this.setState({
      currentJob: null,
      currentIndex: -1
    });
  }

  setActiveJob(job, index) {
    this.setState({
      currentJob: job,
      currentIndex: index
    });
  }

  removeAllJobs() {
    JobService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }
  repeatJob(schedule) {
    console.log("addjob", schedule);
    jobService.create({triggeredBy: "ADMIN", scheduleId: schedule}).then(response => {
      this.retrieveJobs();
    })
    .catch(e => {
      console.error(e);
    });

  }
  render() {
    const { jobs, currentJob } = this.state;

    return (
      <div className="list row">
        <div className="col-md-8">
        <h4 style = {{display: 'inline-block'}}>Jobs</h4>
          {jobs?.length > 0 && <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllJobs} >Remove All</button> 
          }
          <ul className="list-group">
            {jobs && (<table className="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Marketplace</th>
                  <th scope="col">Triggered By</th>
                  <th scope="col">Category</th>
                  <th scope="col">Type</th>
                  <th scope="col">Status</th>
                  <th scope="col">Execution Date</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
              {jobs.map((job, index) => (
                <tr>
                <th scope="row">{index}</th>
                <td>{job.marketplaceName}</td>
                <td>{job.triggeredBy}</td>
                <td>{job.categoryName}</td>
                <td>{job.type}</td>
                <td>{job.status}</td>
                <td>{job.execution_date}</td>
                <td><button className="btn btn-success" onClick={() => this.repeatJob(job.schedule_id)}>
               Repeat
             </button></td>
              </tr>

              )) }
                           </tbody>
          </table>
            )
              }
          </ul>
        </div>
        <div className="col-md-10">
          {currentJob && 
            <div>
              <h4>Job</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>
                {currentJob.name}
              </div>
              <div>
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {currentJob.homepage}
              </div>

              <Link
                to={"/job/" + currentJob.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
            }
         
        </div>
      </div>
    );
  }
}
