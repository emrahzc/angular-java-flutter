import React, { Component } from "react";
import marketplaceService from "../../services/marketplace.service";
import ProductListPageService from "../../services/product-list-page.service";

export default class ProductListPage extends Component {
  state = {
    currentProductListPage: {},
    message: "",
    marketplaces: []
  };
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.getProductListPage = this.getProductListPage.bind(this);
    this.updateProductListPage = this.updateProductListPage.bind(this);
    this.deleteProductListPage = this.deleteProductListPage.bind(this);
    console.log("mymarket:", this.props.match.params.id);
  
  }

  componentDidMount() {
    this.getProductListPage(this.props.match.params.id);
    marketplaceService.getAll().then(response => {
      this.setState({
        marketplaces: response.data.marketplaces
      });
      console.log(response);
    })
    .catch(e => {
      console.log(e);
    });
  }

  onChange(e) {
    this.setState(prevState => ({
      currentProductListPage: {
        ...prevState.currentProductListPage,
        [e.target.name]: e.target.value
      }
    }));
  }

  getProductListPage(id) {
    ProductListPageService.get(id)
      .then(response => {
        this.setState({
          currentProductListPage: response.data.productListPages[0]
        });
        console.log("response:", response);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateProductListPage() {
    ProductListPageService.update(
      this.state.currentProductListPage.id,
      this.state.currentProductListPage
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The productListPage was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteProductListPage() {    
    ProductListPageService.delete(this.state.currentProductListPage.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/productListPages')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentProductListPage } = this.state;
    console.log("currentProductListPage", currentProductListPage)
    return (
      this.state.currentProductListPage ? (
         <div className="row">
        <div className="edit-form col-md-6">
          <h4>ProductListPage</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Path</label>
              <input
                type="text"
                className="form-control"
                id="path"
                defaultValue={currentProductListPage.path }
                onChange={this.onChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="marketplaceId">Marketplace</label>
              <select name="marketplaceId" className="form-control" defaultValue = {currentProductListPage.marketplace_id} onChange={this.onChange}>
                {this.state.marketplaces.map(marketplace =>
                  <option key={marketplace.id} value={marketplace.id}>{marketplace.name}  ({marketplace.homepage})</option>
                  )}
              </select>
            </div>
          </form>

          <button
            className="badge badge-danger mr-2"
            onClick={this.deleteProductListPage}>Delete</button>

          <button type="submit" className="badge badge-success" onClick={this.updateProductListPage}>Update</button>
          <p>{this.state.message}</p>
        </div>
         </div>      ) : (
          <div>
            <br />
            <p>Please click on an item...</p>
          </div>
        )
    );
  }
}
