import React from "react";
import ProductListPageService from "../../services/product-list-page.service";

export default class AddProductListPage extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.saveProductListPage = this.saveProductListPage.bind(this);
    this.newProductListPage = this.newProductListPage.bind(this);

    this.state = {
      id: null,
      path: "",
      submitted: false,
      productListPages: [],
      productListPageId: null,
      type: ''
    };
  }
  componentDidMount() {
    ProductListPageService.getAll().then(response => {
        this.setState({
          productListPages: response.data.rows || []
        });
        console.log(response);
      })
      .catch(e => {
        console.log(e);
      });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  saveProductListPage() {
    ProductListPageService.create( {
      path: this.state.path,
      type: this.state.type,
      productListPageId: this.state.productListPageId
    })
      .then(response => {
        this.setState({
          id: response.data.id,
          path: response.data.path,
          type: response.data.type,
          submitted: true
        });
        console.log(response.data);
        this.props.retrieveProductListPages();
      })
      .catch(e => {
        console.log(e);
      });
  }

  newProductListPage() {
    this.setState({
      id: null,
      path: "",
      productListPageId: null,
      type: '',
      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>Product List Page Added!</h4>
            <button className="btn btn-success" onClick={this.newProductListPage}>
              Add New Product List Page
            </button>
          </div>
        ) : (
          <div>
              <h4>Add new Product List Page</h4>
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                id="path"
                required
                placeholder="enter URL..."
                value={this.state.path}
                onChange={this.onChange}
                name="path"
              />
            </div>

            <div className="form-group">
              <select name="type" className="form-control" defaultValue={this.state.type} onChange={this.onChange}>
              <option value='' disabled>select product type...</option>
                <option value="product">product</option>
                <option value="category">category</option>
              </select>
            </div>

            <button onClick={this.saveProductListPage} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}
