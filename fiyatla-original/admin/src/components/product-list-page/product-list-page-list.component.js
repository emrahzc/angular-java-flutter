import React, { Component } from "react";
import ProductListPageService from "../../services/product-list-page.service";
import { Link } from "react-router-dom";
import AddProductListPage from "./add-product-list-page.component";

export default class ProductListPageList extends Component {
  constructor(props) {
    super(props);
    this.retrieveProductListPages = this.retrieveProductListPages.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveProductListPage = this.setActiveProductListPage.bind(this);
    this.removeAllProductListPages = this.removeAllProductListPages.bind(this);

    this.state = {
      productListPages: [],
      currentProductListPage: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveProductListPages();
  }

  retrieveProductListPages() {
    ProductListPageService.getAll().then(response => {
        this.setState({
          productListPages: response.data.rows || []
        });
      })
      .catch(e => {
        console.error(e);
      });
  }

  refreshList() {
    this.retrieveProductListPages();
    this.setState({
      currentProductListPage: null,
      currentIndex: -1
    });
  }

  setActiveProductListPage(productListPage, index) {
    this.setState({
      currentProductListPage: productListPage,
      currentIndex: index
    });
  }

  removeAllProductListPages() {
    ProductListPageService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.error(e);
      });
  }

  render() {
    const { productListPages, currentProductListPage, currentIndex } = this.state;
    return (
      <div className="list row">
        <div className="col-md-8">
          <h4 style = {{display: 'inline-block'}}>Product List Pages</h4>
          {productListPages.length > 0 && <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllProductListPages}
          >
            Remove All
          </button> }

          <ul className="list-group">
            {productListPages &&
              productListPages.map((productListPage, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveProductListPage(productListPage, index)}
                  key={index}
                >
                    {( productListPage.homepage ?? this.props.marketplace?.homepage) + productListPage.path}
                </li>
              ))}
          </ul>

        </div>
        <div className="col-md-8">
          {currentProductListPage ? (
            <div>
              <h4>ProductListPage</h4>
              <div>
                <label>
                  <strong>Path:</strong>
                </label>
                {currentProductListPage.homepage + currentProductListPage.path}
              </div>
              <div>
                <label>
                  <strong>Type:</strong>
                </label>{currentProductListPage.type}
                {}
              </div>

              <Link
                to={"/productListPage/" + currentProductListPage.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href={currentProductListPage.homepage + currentProductListPage.path}
                className="badge badge-warning"
              >
                Visit
              </a>
            </div>
          ) : ""
          }
          <AddProductListPage retrieveProductListPages={this.retrieveProductListPages} />
        </div>
      </div>
    );
  }
}
