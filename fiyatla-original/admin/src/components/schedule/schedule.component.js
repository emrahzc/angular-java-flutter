import React, { Component } from "react";
import ScheduleService from "../../services/schedule.service";
import SchedulesList from "./schedule-list.component";

export default class Schedule extends Component {
  state = {
    currentSchedule: {},
    message: ""
  };
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeHomePage = this.onChangeHomePage.bind(this);
    this.getSchedule = this.getSchedule.bind(this);
    this.updatePublished = this.updatePublished.bind(this);
    this.updateSchedule = this.updateSchedule.bind(this);
    this.deleteSchedule = this.deleteSchedule.bind(this);
    console.log("mymarket:", this.props.match.params.id);
  
  }

  componentDidMount() {
    this.getSchedule(this.props.match.params.id);
  }

  onChangeTitle(e) {
    const name = e.target.value;

    this.setState(function(prevState) {
      return {
        currentSchedule: {
          ...prevState.currentSchedule,
          name: name
        }
      };
    });
  }

  onChangeHomePage(e) {
    const homepage = e.target.value;

    this.setState(prevState => ({
      currentSchedule: {
        ...prevState.currentSchedule,
        homepage: homepage
      }
    }));
  }

  getSchedule(id) {
    ScheduleService.get(id)
      .then(response => {
        this.setState({
          currentSchedule: response.data.marketplaces[0]
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePublished(status) {
    var data = {
      id: this.state.currentSchedule.id,
      name: this.state.currentSchedule.name,
      homepage: this.state.currentSchedule.homepage,
      published: status
    };

    ScheduleService.update(this.state.currentSchedule.id, data)
      .then(response => {
        this.setState(prevState => ({
          currentSchedule: {
            ...prevState.currentSchedule,
            published: status
          }
        }));
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateSchedule() {
    ScheduleService.update(
      this.state.currentSchedule.id,
      this.state.currentSchedule
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The marketplace was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteSchedule() {    
    ScheduleService.delete(this.state.currentSchedule.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/marketplaces')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentSchedule } = this.state;
    console.log("render",currentSchedule);
    return (
        currentSchedule ? (
         <div className="row">

    <div className="edit-form col-md-3" style={{    backgroundColor: '#454b5a24'}}>
          <h4>Schedule</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Title</label>
              <input
                type="text"
                className="form-control"
                id="name"
                defaultValue={currentSchedule.name }
                onChange={this.onChangeTitle}
              />
            </div>
            <div className="form-group">
              <label htmlFor="homepage">HomePage</label>
              <input
                type="text"
                className="form-control"
                id="homepage"
                defaultValue={currentSchedule.homepage}
                onChange={this.onChangeHomePage}
              />
            </div>

            <div className="form-group">
              <label>
                <strong>Status:</strong>
              </label>
              {currentSchedule.published ? "Published" : "Pending"}
            </div>
          </form>

          {currentSchedule.published ? (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(false)}
            >
              UnPublish
            </button>
          ) : (
            <button
              className="badge badge-primary mr-2"
              onClick={() => this.updatePublished(true)}
            >
              Publish
            </button>
          )}

          <button
            className="badge badge-danger mr-2"
            onClick={this.deleteSchedule}
          >
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={this.updateSchedule}
          >
            Update
          </button>
          <p>{this.state.message}</p>
        </div>
 
 
        <div className="col-md-9">
           {currentSchedule.id && <SchedulesList marketplace = {currentSchedule} /> }
        </div>
 
         </div>      ) : ""
    );
  }
}
