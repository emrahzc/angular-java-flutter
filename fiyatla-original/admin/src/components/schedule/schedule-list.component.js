import React, { Component } from "react";
import ScheduleService from "../../services/schedule.service";
import { Link } from "react-router-dom";
import AddSchedule from "./add-schedule.component";
import jobService from "../../services/job.service";

export default class SchedulesList extends Component {
  constructor(props) {
    super(props);
    this.retrieveSchedules = this.retrieveSchedules.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveSchedule = this.setActiveSchedule.bind(this);
    this.removeAllSchedules = this.removeAllSchedules.bind(this);
    this.addJob = this.addJob.bind(this);

    this.state = {
      schedules: [],
      currentSchedule: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveSchedules();
  }

  retrieveSchedules() {
    ScheduleService.getAll()
      .then(response => {
        this.setState({
          schedules: response.data.rows
        });
      })
      .catch(e => {
        console.error(e);
      });
  }

  refreshList() {
    this.retrieveSchedules();
    this.setState({
      currentSchedule: null,
      currentIndex: -1
    });
  }

  setActiveSchedule(schedule, index) {
    this.setState({
      currentSchedule: schedule,
      currentIndex: index
    });
  }
  addJob(schedule) { 
    console.log("addjob", schedule);
    jobService.create({triggeredBy: "ADMIN", scheduleId: schedule}).then(response => {
      this.props.history.push('/jobs')
    })
    .catch(e => {
      console.error(e);
    });

  }

  removeAllSchedules() {
    ScheduleService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { schedules, currentSchedule, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-8">
        <h4 style = {{display: 'inline-block'}}>Schedules</h4>
          {schedules?.length > 0 && <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllSchedules} >Remove All</button> 
          }
          <ul className="list-group">
            {schedules && schedules.length > 0 && (
              <table className="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Marketplace</th>
                  <th scope="col">Creator</th>
                  <th scope="col">Type</th>
                  <th scope="col">Category</th>
                  <th scope="col">Cron</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
              {schedules.map((schedule, index) => (
                 <tr key = {'schedules#'+index}>
                 <th scope="row">{index}</th>
                 <td>{schedule.marketplaceName}</td>
                 <td>{schedule.created_by}</td>
                 <td>{schedule.type}</td>
                 <td>{schedule.categoryName}</td>
                 <td>{schedule.cron}</td>
                 <td><button className="btn btn-success" onClick={() => this.addJob(schedule.id)}>
                Run
              </button></td>
               </tr>
            ))}
             </tbody>
          </table>
            )}
          </ul>
        </div>
        <div className="col-md-8">
          {currentSchedule && 
            <div>
              <h4>Schedule</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>
                {currentSchedule.name}
              </div>
              <div>
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {currentSchedule.homepage}
              </div>

              <Link
                to={"/schedule/" + currentSchedule.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
            }
            <AddSchedule retrieveSchedules={this.retrieveSchedules} />
         
        </div>
      </div>
    );
  }
}
