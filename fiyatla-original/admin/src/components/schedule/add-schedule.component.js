import React from "react";
import categoryService from "../../services/category.service";
import marketplaceService from "../../services/marketplace.service";
import productListPageService from "../../services/product-list-page.service";
import ScheduleService from "../../services/schedule.service";
import { TypeAhead } from "../../widgets/TypeAhead";

export default class AddSchedule extends React.Component {
  constructor(props) {
    super(props);
    this.onCategoryChange = this.onCategoryChange.bind(this);
    this.onChange = this.onChange.bind(this);
    this.saveSchedule = this.saveSchedule.bind(this);
    this.newSchedule = this.newSchedule.bind(this);
    this.checkProductListPage = this.checkProductListPage.bind(this);
    this.newPage = this.newPage.bind(this);

    this.state = {
      id: null,
      createdBy: "",
      cron: "", 
      type: "",
      submitted: false,
      marketplaces: [],
      category: null,
      marketplaceId: null,
      productListPage: null,
      pageUrl: ""
    };
  }

  onChange(e) {
    const name = e.target.name
    this.setState({
      [name]: e.target.value
    }, () => {
      if(name === 'category' || name === 'type' || name === 'marketplaceId'){
        this.checkProductListPage();
      }  
    });
  }

  onCategoryChange(value) {
    console.log("new category", value)
    this.setState({
      category: value
    });
    if(value){
      this.checkProductListPage();
    }
  }

  componentDidMount() {
    marketplaceService.getAll().then(response => {
        this.setState({
          marketplaces: response.data.rows
        });
      })
      .catch(e => {
        console.error(e);
      });
  }
  
  newPage() {
    const pathArray = this.state.pageUrl?.split("/");
    console.log(pathArray);
    const path = pathArray.slice(-2).join('/');
    productListPageService.create({marketplaceId: this.state.marketplaceId, categoryId: this.state.category, path}).then(response => {
        this.checkProductListPage();
      })
      .catch(e => {
        console.error(e);
      });
  }

  checkProductListPage() {
    if(this.state.type !== 'LIST' || this.state.category == null || this.state.marketplaceId == null){
      console.log("not list!", this.state.type, this.state.category, this.state.marketplaceId);
      return;
    }
    console.log("checkProductListPage");
    productListPageService.search({marketplaceId: this.state.marketplaceId, categoryId: this.state.category}).then(response => {
      console.log("checkProductListPage", response);
      this.setState({
          productListPage: response.data && response.data.rows && response.data.rows.length > 0 ? response.data.rows[0] : -1,
          pageUrl: response.data && response.data.rows && response.data.rows.length > 0 ? response.data.rows[0].path : ""
        });
      })
      .catch(e => {
        console.error(e);
        this.setState({productListPage:-1});
      })
  }
  saveSchedule() {
    console.log("saveSchedule", this.state.productListPage);
    var data = {
      createdBy: "ADMIN",
      cron: this.state.cron,
      type: this.state.type,
      marketplaceId: this.state.marketplaceId,
      categoryId: this.state.category
    };

    ScheduleService.create(data)
      .then(response => {
        this.setState({
          submitted: true
        });
        this.props.retrieveSchedules();
      })
      .catch(e => {
        console.log(e);
      });
  }

  newSchedule() {
    this.setState({
      id: null,
      pageUrl: "",
      cron: "",
      type: "",
      submitted: false,
      category: null,
      marketplaceId: null
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={this.newSchedule}>
              Add New Schedule
            </button>
          </div>
        ) : (
          <div>
            <br />
            <br />
            <h4>new schedule</h4>

            <div className="form-group">
              <select name="marketplaceId" className="form-control" defaultValue = '' onChange={this.onChange}>
                <option value='' disabled >select marketplace...</option>
                {this.state.marketplaces.map(marketplace =>
                  <option key={marketplace.id} value={marketplace.id}>{marketplace.name}  ({marketplace.homepage})</option>
                  )}
              </select>
            </div>
              <div className="form-group">
                <TypeAhead searchService = {categoryService.searchByName} entityName = "category" onChange={this.onCategoryChange} />
              </div>
            <div className="form-group">
              <select name="type" disabled = {this.state.marketplaceId === null || this.state.category === null} 
              className="form-control" defaultValue={this.state.type} onChange={this.onChange}>
              <option value='' disabled>select target type...</option>
                <option value="PRODUCT">product</option>
                <option value="LIST">list</option>
              </select>
            </div>

            {this.state.productListPage !== null && <div className="form-group">
              <p>{this.state.productListPage !== null && (this.state.productListPage !== -1 ? "page found!" : "page not found!")} </p>
              <input
                type="text"
                disabled = {this.state.productListPage !== -1}
                className="form-control"
                placeholder = "enter page address..."
                id="pageUrl"
                required
                value={this.state.pageUrl}
                onChange={this.onChange}
                name = "pageUrl"
              />
              {
               this.state.productListPage === -1 && (<button className="btn btn-success" onClick={this.newPage}>
                Add
              </button>)
              }
            </div>}

            <div className="form-group">
              <input
                type="text"
                className="form-control"
                id="cron"
                placeholder = "enter cron..."
                required
                value={this.state.cron}
                name = "cron"
                onChange={this.onChange}
              />
            </div>

            <button onClick={this.saveSchedule} disabled = {(!this.state.productListPage  && this.state.type !== 'PRODUCT') || !this.state.cron} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}
