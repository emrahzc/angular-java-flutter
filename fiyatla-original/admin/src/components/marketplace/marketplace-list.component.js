import React, { Component } from "react";
import MarketplaceService from "../../services/marketplace.service";
import { Link } from "react-router-dom";
import AddMarketplace from "./add-marketplace.component";

export default class MarketplacesList extends Component {
  constructor(props) {
    super(props);
    this.retrieveMarketplaces = this.retrieveMarketplaces.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveMarketplace = this.setActiveMarketplace.bind(this);
    this.removeAllMarketplaces = this.removeAllMarketplaces.bind(this);

    this.state = {
      marketplaces: [],
      currentMarketplace: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveMarketplaces();
  }

  retrieveMarketplaces() {
    MarketplaceService.getAll()
      .then(response => {
        this.setState({
          marketplaces: response.data.rows
        });
        console.log(response.marketplaces);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveMarketplaces();
    this.setState({
      currentMarketplace: null,
      currentIndex: -1
    });
  }

  setActiveMarketplace(marketplace, index) {
    this.setState({
      currentMarketplace: marketplace,
      currentIndex: index
    });
  }

  removeAllMarketplaces() {
    MarketplaceService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { marketplaces, currentMarketplace, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-8">
        <h4 style = {{display: 'inline-block'}}>Marketplaces</h4>
          {marketplaces?.length > 0 && <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllMarketplaces} >Remove All</button> 
          }
          <ul className="list-group">
            {marketplaces &&
              marketplaces.map((marketplace, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveMarketplace(marketplace, index)}
                  key={index}
                >
                  {marketplace.homepage}
                <Link to={"/marketplace/" + marketplace.id} className="badge badge-warning" style ={{float:"right"}}>
                    {marketplace.name}
                </Link>
                </li>
              ))}
          </ul>
        </div>
        <div className="col-md-8">
          {currentMarketplace && 
            <div>
              <h4>Marketplace</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>
                {currentMarketplace.name}
              </div>
              <div>
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {currentMarketplace.homepage}
              </div>

              <Link
                to={"/marketplace/" + currentMarketplace.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
            }
            <AddMarketplace retrieveMarketplaces={this.retrieveMarketplaces} />
         
        </div>
      </div>
    );
  }
}
