import React from "react";
import MarketplaceService from "../../services/marketplace.service";

export default class AddMarketplace extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeHomepage = this.onChangeHomepage.bind(this);
    this.saveMarketplace = this.saveMarketplace.bind(this);
    this.newMarketplace = this.newMarketplace.bind(this);

    this.state = {
      id: null,
      name: "",
      homepage: "", 
      published: false,

      submitted: false
    };
  }

  onChangeName(e) {
    this.setState({
      name: e.target.value
    });
  }

  onChangeHomepage(e) {
    this.setState({
      homepage: e.target.value
    });
  }

  saveMarketplace() {
    var data = {
      name: this.state.name,
      homepage: this.state.homepage
    };

    MarketplaceService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          name: response.data.name,
          homepage: response.data.homepage,
          published: response.data.published,

          submitted: true
        });
        this.props.retrieveMarketplaces();
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  newMarketplace() {
    this.setState({
      id: null,
      name: "",
      homepage: "",
      published: false,

      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={this.newMarketplace}>
              Add
            </button>
          </div>
        ) : (
          <div>
            <br />
            <br />
            <h4>new marketplace</h4>
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                placeholder = "enter name..."
                id="name"
                required
                value={this.state.name}
                onChange={this.onChangeName}
                name="name"
              />
            </div>

            <div className="form-group">
              <input
                type="text"
                className="form-control"
                id="homepage"
                placeholder = "enter home page..."
                required
                value={this.state.homepage}
                onChange={this.onChangeHomepage}
                name="homepage"
              />
            </div>

            <button onClick={this.saveMarketplace} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}
