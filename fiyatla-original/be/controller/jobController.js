const { mysqlConnection, query } = require('../util/mysql');
const handleError = require('../util/handle-error');
const { urlScrapeProducer, priceScrapeProducer } = require('../server');

module.exports = (express) => {
	const router   = express.Router();
	const selectQuery = "SELECT job.*, mp.name as marketplaceName, ct.name as categoryName, sc.type as type "
	+"from job, schedule sc, marketplace mp, category ct "
	+"where job.schedule_id=sc.id and ct.id=sc.category_id and sc.marketplace_id = mp.id ";
	router.get('/', (req, res) => {
		console.log("SELECT * from job LIMIT 0, 10")
	    mysqlConnection.query(selectQuery + ' order by job.execution_date LIMIT 0, 10', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.delete('/', (req, res) => {
	    mysqlConnection.query('delete from job', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/:jobId', (req, res) => {
	    mysqlConnection.query(selectQuery + 'and id = ?', [req.params.jobId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/offset/:offset', (req, res) => {
	    mysqlConnection.query(selectQuery + 'LIMIT ?, ?', [req.params.offset, req.params.offset + 20], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    })
	})

	router.post('/', (req, res) => {
	    mysqlConnection.query('insert into job (triggered_by, schedule_id) values(?, ?)', 
			[req.body.triggeredBy, req.body.scheduleId], async (err, result, fields) => {
	        if (err) return handleError(err, res)
			const schedule = (await query('SELECT * from schedule where id = ?', [req.body.scheduleId]))[0]
			let producer
			if(schedule.type === 'LIST'){
				producer = urlScrapeProducer
			}else{
				producer = priceScrapeProducer
			}
			console.log("started producing")
			producer.produceMessage({ jobId: result.insertId }, (err) => {
				if (err) return handleError(err, res)
				console.log("finished producing")
			});
			res.jsonp({'id': result.insertId})
	    })
	})
	return router
};