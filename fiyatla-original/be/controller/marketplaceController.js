const { mysqlConnection } = require('../util/mysql');
const handleError = require('../util/handle-error');

module.exports = (express) => {
	const router      = express.Router();

	router.get('/', (req, res) => {
		console.log("SELECT * from marketplace LIMIT 0, 10")
	    mysqlConnection.query('SELECT * from marketplace LIMIT 0, 10', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/:marketplaceId', (req, res) => {
	    mysqlConnection.query('SELECT * from marketplace where id = ?', [req.params.marketplaceId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.delete('/:marketplaceId', (req, res) => {
	    mysqlConnection.query('DELETE from marketplace where id = ?', [req.params.marketplaceId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/offset/:offset', (req, res) => {
	    mysqlConnection.query('SELECT * from marketplace LIMIT ?, ?', [req.params.offset, req.params.offset + 20], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.post('/', (req, res) => {
	    mysqlConnection.query('insert into marketplace (name, homepage) values(?, ?)', [req.body.name, req.body.homepage], (err, result, fields) => {
	        if (err) return handleError(err, res);
			res.jsonp({'id': result.insertId});
	    }); 
	});
	return router;
};