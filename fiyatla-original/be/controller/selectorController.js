const { mysqlConnection, query } = require('../util/mysql');
const { createSelector, createSelectorGroup, deleteOldSelectors } = require('../service/selectorService');
const { getProductListId } = require('../service/urlService');
const handleError = require('../util/handle-error');
const { getProductId } = require('../service/productService');
const { getMarketplaceId } = require('../service/marketplaceService');
const { getCategoryId } = require('../service/categoryService');



module.exports = (express) => {
	const router = express.Router();

	router.get('/', (req, res) => {
		console.log("SELECT * from selector LIMIT 0, 10")
	    mysqlConnection.query('SELECT * from selector LIMIT 0, 10', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/:selectorId', (req, res) => {
	    mysqlConnection.query('SELECT * from selector where id = ?', [req.params.selectorId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/offset/:offset', (req, res) => {
	    mysqlConnection.query('SELECT * from selector LIMIT ?, ?', [req.params.offset, req.params.offset + 20], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.post('/extension', async (req, res) =>  {
		getProductListId(req.body.url, req.body.category);
		let url = new URL(req.body.url);
		let marketplaceId = await getMarketplaceId(url.origin)
        const categoryId = await getCategoryId(req.body.category)

		await deleteOldSelectors(req.body.type, marketplaceId, categoryId)
		const selectorGroupId = await createSelectorGroup(req.body.type, marketplaceId, categoryId)
		const ids = []
		for(let selector of req.body.selectors){
			ids.push(await createSelector(selectorGroupId,selector.type, selector.selector))
		}
		res.jsonp({ids}); 
	});

	return router;
};