const { Message } = require('redis-smq');
const { mysqlConnection } = require('../util/mysql');
const handleError = require('../util/handle-error');

module.exports = (express) => {
	const router      = express.Router();
	const selectQuery = "SELECT sc.*, ct.name as categoryName, mp.name as marketplaceName "
			+"from schedule sc, category ct, marketplace mp where mp.id = sc.marketplace_id and sc.category_id = ct.id";

	router.get('/', (req, res) => {
	    mysqlConnection.query(selectQuery +' LIMIT 0, 10', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	
	router.get('/:scheduleId', (req, res) => {
	    mysqlConnection.query(selectQuery+' and sc.id = ?', [req.params.scheduleId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/offset/:offset', (req, res) => {
	    mysqlConnection.query(selectQuery+ ' LIMIT ?, ?', [req.params.offset, req.params.offset + 20], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.post('/', (req, res) => {
	    mysqlConnection.query('insert into schedule (cron, marketplace_id, category_id, type, created_by) values(?, ?, ?, ?, ?)', [req.body.cron, req.body.marketplaceId, req.body.categoryId, req.body.type, req.body.createdBy], (err, result, fields) => {
	        if (err) return handleError(err, res);
			const msg = new Message();
			msg.setBody({ scheduleId: result.insertId });
			
			res.jsonp({'id': result.insertId});
	    });
	});
	return router;
};