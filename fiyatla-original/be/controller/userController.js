const { mysqlConnection } = require('../util/mysql');
const handleError = require('../util/handle-error');

module.exports = (express) => {
	const router = express.Router();

	router.post('/login', (req, res) => {
		if(process.env.ADMIN_PASS === req.body.pass && req.body.user === process.env.ADMIN_USER){
			res.json(require('../util/jwt')({ username: req.body.user }));
			return;
		}
		res.json("unauthorized");
	});

	return router;
};