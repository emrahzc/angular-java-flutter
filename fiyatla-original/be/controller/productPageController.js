const { mysqlConnection } = require('../util/mysql');
const handleError = require('../util/handle-error');

module.exports = (express) => {
	const router = express.Router();

	router.get('/', (req, res) => {
	    mysqlConnection.query('SELECT pg.*, p.name as product_name, mp.homepage as homepage from marketplace mp, product_list_page plp, product_page pg, product p '+
				'where mp.id = plp.marketplace_id and plp.id = pg.parent_id and pg.product_id= p.id LIMIT 0, 10', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/:productPageId', (req, res) => {
	    mysqlConnection.query('SELECT pg.*, p.name from product_page pg, product p where pg.product_id= p.id and pg.id = ?', [req.params.productPageId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/product/:productId', (req, res) => {
	    mysqlConnection.query('SELECT * from product_page where product_id = ?', [req.params.productId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/offset/:offset', (req, res) => {
	    mysqlConnection.query('SELECT product_page.*, p.name from product_page pg, product p where pg.product_id= p.id LIMIT ?, ?', [req.params.offset, req.params.offset + 20], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.post('/', (req, res) => {
	    mysqlConnection.query('insert into product_page (path, parent_id, product_id) values(?, ?, ?)', [req.body.path, req.body.parentId, req.body.productId], (err, result, fields) => {
	        if (err) return handleError(err, res);
			res.jsonp({'id': result.insertId});
	    }); 
	});
	return router;
};