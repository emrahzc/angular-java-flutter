const jwt = require('jsonwebtoken');


const callNext = (next, res) => {
    try{
        res.header("Access-Control-Allow-Origin", "*"); 
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next()
    }catch(error){
        console.error(error)
        res.sendStatus(500)
    }
}

module.exports = (express) => {
    let router = express.Router({mergeParams: true})
    
    router.use((req, res, next) => {
        if(req.url === "/auth/login"){
            return callNext(next, res)
        }else if(req.url === "/selector/extension"){
            const b64auth = (req.body.Authorization || '').split(' ')[1] || ''
            const [login, password] = Buffer.from(b64auth, 'base64').toString().split('|')
            if (login && password && login === process.env.EXTENSION_USER && password === process.env.EXTENSION_PASS) {
                return callNext(next, res)
                               
            }
            return res.sendStatus(401)
        }else if ('OPTIONS' == req.method) {
            return res.sendStatus(200)
        }
        
        if('GET' === req.method) {
            return callNext(next, res) //TODO remove it after adding user authorization
        }

        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]
        if (token == null) return res.sendStatus(401)
    
        jwt.verify(token, process.env.SECRET_TOKEN, (err, user) => {
          console.log(err)
      
          if (err) return res.sendStatus(403)
          req.user = user
          callNext(next, res)
        })  
    });

    const productRouter = require('./productController')(express);
    const categoryRouter = require('./categoryController')(express);
    const jobRouter = require('./jobController')(express);
    const scheduleRouter = require('./scheduleController')(express);
    const sellerRouter = require('./sellerController')(express);
    const selectorRouter = require('./selectorController')(express);
    const marketplaceRouter = require('./marketplaceController')(express);
    const productPageRouter = require('./productPageController')(express);
    const productListPageRouter = require('./productListPageController')(express);
    const authRouter = require('./userController')(express);
    router.use('/product', productRouter);
    router.use('/category', categoryRouter);
    router.use('/job', jobRouter);
    router.use('/schedule', scheduleRouter);
    router.use('/seller', sellerRouter);
    router.use('/selector', selectorRouter);
    router.use('/marketplace', marketplaceRouter);
    router.use('/productPage', productPageRouter);
    router.use('/productListPage', productListPageRouter);
    router.use('/auth', authRouter);

    return router
  }