const { mysqlConnection } = require('../util/mysql');
const handleError = require('../util/handle-error');

module.exports = (express) => {
	const router      = express.Router();

	router.get('/', (req, res) => {
		console.log("SELECT * from category LIMIT 0, 10")
	    mysqlConnection.query('SELECT * from category LIMIT 0, 10', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/:categoryId', (req, res) => {
	    mysqlConnection.query('SELECT * from category where id = ?', [req.params.categoryId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/:categoryId', (req, res) => {
	    mysqlConnection.query('SELECT * from category where id = ?', [req.params.categoryId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/search/:name', (req, res) => {
	    mysqlConnection.query('SELECT * from category where name like ? limit 0,20', [ '%' + req.params.name + '%'], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    });
	});

	router.get('/offset/:offset', (req, res) => {
	    mysqlConnection.query('SELECT * from category LIMIT ?, ?', [req.params.offset, req.params.offset + 20], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.post('/', (req, res) => {
	    mysqlConnection.query('insert into category (name) values(?)', [req.body.name], (err, result, fields) => {
	        if (err) return handleError(err, res);
			res.jsonp({'id': result.insertId});
	    }); 
	});
	return router;
};