const { mysqlConnection, query } = require('../util/mysql');
const handleError = require('../util/handle-error');

module.exports = (express) => {
	const router      = express.Router();

	router.get('/', (req, res) => {
	    mysqlConnection.query('SELECT * from product LIMIT 0, 10', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/:productId', (req, res) => {
	    mysqlConnection.query('SELECT * from product where id = ?', [req.params.productId], async (err, rows, fields) => {
	        if (err) return handleError(err, res);
			const prices = await query('select price.price, seller.name as seller_name, seller.id as seller_id, price.marketplace_id from price, seller '+
			' where price.seller_id = seller.id and price.product_id = ? order by price.price asc', [rows[0].id])
			if(prices && prices.length>0){
				rows[0].price = prices[0].price
				rows[0].seller_name = prices[0].seller_name
				rows[0].marketplace_id = prices[0].marketplace_id
				rows[0].seller_id = prices[0].seller_id
			}
	        res.jsonp({rows});
	    }); 
	});

	router.get('/offset/:offset', (req, res) => {
	    mysqlConnection.query('SELECT * from product LIMIT ?, ?', [req.params.offset, req.params.offset + 20], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/category/:categoryId', (req, res) => {
	    mysqlConnection.query('SELECT * from product where category_id = ?', [req.params.categoryId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.post('/', (req, res) => {
	    mysqlConnection.query('insert into product (name, fullname, category_id) values(?,?,?)', [req.body.name, req.body.fullname, req.body.categoryId], (err, result, fields) => {
			if (err) return handleError(err, res);
			res.jsonp({'id': result.insertId});
	    }); 
	});
	return router;
};