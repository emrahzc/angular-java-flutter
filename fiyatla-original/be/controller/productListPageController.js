const { mysqlConnection } = require('../util/mysql');
const handleError = require('../util/handle-error');

module.exports = (express) => {
	const router = express.Router();
	router.get('/', (req, res) => {
	    mysqlConnection.query('SELECT pg.*, mp.homepage from product_list_page pg, marketplace mp where pg.marketplace_id= mp.id LIMIT 0, 10', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/:productListPageId', (req, res) => {
	    mysqlConnection.query('SELECT pg.*, mp.homepage from product_list_page pg, marketplace mp where pg.marketplace_id= mp.id and pg.id = ?', [req.params.productListPageId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/marketplace/:marketplaceId', (req, res) => {
	    mysqlConnection.query('SELECT * from product_list_page where marketplace_id = ?', [req.params.marketplaceId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/offset/:offset', (req, res) => {
	    mysqlConnection.query('SELECT product_list_page.*, mp.homepage from product_list_page pg, marketplace mp where pg.marketplace_id= mp.id LIMIT ?, ?', [req.params.offset, req.params.offset + 20], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.post('/', (req, res) => {
	    mysqlConnection.query('insert into product_list_page (path, marketplace_id, category_id) values(?, ?, ?)', [req.body.path, req.body.marketplaceId, req.body.categoryId], (err, result, fields) => {
	        if (err) return handleError(err, res);
			res.jsonp({'id': result.insertId});
	    }); 
	});

	router.post('/search', (req, res) => {
	    mysqlConnection.query('SELECT * from product_list_page where marketplace_id = ? and category_id = ?', [req.body.marketplaceId, req.body.categoryId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});
	return router;
};