const { mysqlConnection } = require('../util/mysql');
const handleError = require('../util/handle-error');

module.exports = (express) => {
	const router      = express.Router();

	router.get('/', (req, res) => {
		console.log("SELECT * from seller LIMIT 0, 10")
	    mysqlConnection.query('SELECT * from seller LIMIT 0, 10', (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/:sellerId', (req, res) => {
	    mysqlConnection.query('SELECT * from seller where id = ?', [req.params.sellerId], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.get('/offset/:offset', (req, res) => {
	    mysqlConnection.query('SELECT * from seller LIMIT ?, ?', [req.params.offset, req.params.offset + 20], (err, rows, fields) => {
	        if (err) return handleError(err, res);
	        res.jsonp({rows});
	    }); 
	});

	router.post('/', (req, res) => {
	    mysqlConnection.query('insert into seller (name, homepage) values(?, ?)', [req.body.name, req.body.homePage], (err, result, fields) => {
	        if (err) return handleError(err, res);
			res.jsonp({'id': result.insertId});
	    }); 
	});
	return router;
};