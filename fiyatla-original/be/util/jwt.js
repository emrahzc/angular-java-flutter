
module.exports = (username) => {
    return require('jsonwebtoken').sign(username, process.env.SECRET_TOKEN, { expiresIn: '86400s' }); 
  }