module.exports = {
    namespace: 'ns1',
    redis: {
        driver: 'redis',
        options: {
            host: process.env.REDIS_HOST,
            port: 6379
        }
    },
    log: {
        enabled: false,
        options: {
            level: 'trace'
        }
    },
    monitor: {
        enabled: true,
        port: 3001,
        host: '0.0.0.0'
    }
};