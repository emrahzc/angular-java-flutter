const util = require('util');
const mysql	= require('mysql');

let	mysqlConnection  = mysql.createConnection({
	host     : process.env.DB_HOST,
	user     : process.env.MYSQL_USER,
	password : process.env.MYSQL_PASSWORD,
	database : process.env.MYSQL_DATABASE
});
mysqlConnection.connect();

const query = util.promisify(mysqlConnection.query).bind(mysqlConnection);
module.exports =  {
	mysqlConnection,
	query
};