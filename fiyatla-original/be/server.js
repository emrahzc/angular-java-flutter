const express     = require('express');
const app         = express();
const path        = require('path');
const bodyParser  = require('body-parser');
const { Producer } = require('redis-smq');
const config = require('./util/redis-config');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const router = require('./controller/router');
var cors = require('cors');
const { startConsumers } = require('./scraper/startConsumers');

 
app.use(cors())
module.exports = {
    urlScrapeProducer: new Producer('url_scrape_queue', config),
    priceScrapeProducer: new Producer('price_scrape_queue', config)
}
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); // use either jade or ejs       
app.use(express.static('public'));

app.set('jsonp callback name', 'callback');

app.use('/api/', router(express))

app.use('/api-docs', swaggerUi.serve)
app.get('/api-docs', swaggerUi.setup(swaggerDocument))


app.get('/', (_, res) => {
    res.render('index')
})
app.disable('x-powered-by')
app.listen(3000, () => {
	console.log('fiyatLa BE listening on port 3000!')
    startConsumers()
})


