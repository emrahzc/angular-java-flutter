var webdriverio = require('webdriverio');


module.exports.scrapeUrlPage =  async (request) => {

    console.log('Starting scraping price page', request)
    const browser = await webdriverio.remote({
        hostname: process.env.SELENIUM_HOST,
        capabilities: { browserName: 'chrome', browserVersion: '27.0'},
        path: '/wd/hub',
        logLevel : 'error'
    })
    let productPages = []
    try {
        await browser.navigateTo(request.page)   
        const nameSelector = request.selectors.filter(sel => sel.selector_type === 'url')[0]
        const nextSelector = request.selectors.filter(sel => sel.selector_type === 'next')[0]
        let maxPage = 0
        while(maxPage<7){ //todo split into batches of 8 
            const names = await browser.$$(nameSelector.value)
            for(let nameDiv of names){
                let href = await nameDiv.getAttribute('href')
                let name = await nameDiv.getHTML(false)
                let curr = nameDiv
                while(!href){
                    curr = await curr.parentElement()
                    href = await curr.getAttribute('href')
                }
                productPages.push({href, name})
                //console.log({href, name})
            }
            const nextBtn = await browser.$(nextSelector.value)
            if((await nextBtn.isExisting())){
                maxPage++
                console.log("next page")
                let currentUrl = await browser.getUrl()
                await nextBtn.click()
                const nextUrl = await browser.getUrl() 
                console.log("maxPage", maxPage, nextUrl)
                if(currentUrl === nextUrl){
                    break
                }
                currentUrl = nextUrl
            }else{
                break
            }
        }
            
    } catch (error) {
        console.error(error)
    }
    console.log("ending browser")
    await browser.deleteSession()
    return productPages;
}
