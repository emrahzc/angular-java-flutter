const redisConfig = require("../util/redis-config");
const { PriceScraperConsumer } = require("./PriceScraperConsumer");
const { UrlScraperConsumer } = require("./UrlScraperConsumer");
const { monitor } = require('redis-smq');

module.exports.startConsumers = () => {
    const urlConsumer = new UrlScraperConsumer(redisConfig, { messageConsumeTimeout: 2000 })
    urlConsumer.run()
    console.log("redis: UrlScraperConsumer started")

    const priceConsumer = new PriceScraperConsumer(redisConfig, { messageConsumeTimeout: 2000 })
    priceConsumer.run()
    console.log("redis: UrlScraperConsumer started")

    monitor(redisConfig).listen(() => {
        console.log(`redis monitor started at ${redisConfig.monitor.host}:${redisConfig.monitor.port}`)
    });        
}