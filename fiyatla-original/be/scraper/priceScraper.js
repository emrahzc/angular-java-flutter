var webdriverio = require('webdriverio');

async function getElements(br, selector){
    let els = await br.$$(selector)
    if(els.length > 0){
        return els
    }
    let tokens = selector.split(/(\d+)/)
    for(let i=tokens.length-2; i>0; i--){
        if(!isNaN(tokens[i])){
            const currentNumber = parseInt(tokens[i])
            tokens[i] = currentNumber-1
            els = await br.$$(tokens.join(''))
            if(els.length > 0){
                return els
            }
            tokens[i] = currentNumber+1
            els = await br.$$(tokens.join(''))
            if(els.length > 0){
                return els
            }
            tokens[i] = currentNumber
        }
    }
    return []
}

module.exports.scrapePricePage =  async (request) => {

    console.log('Starting scraping price page')
    const browser = await webdriverio.remote({
        hostname: process.env.SELENIUM_HOST,
        capabilities: { browserName: 'chrome', browserVersion: '27.0'},
        path: '/wd/hub',
        logLevel : 'error'
    })

    const priceSelector = request.selectors.filter(sel => sel.selector_type === 'price')[0]
    const nextSelectors = request.selectors.filter(sel => sel.selector_type === 'next')
    const sellerSelector = request.selectors.filter(sel => sel.selector_type === 'seller')[0]
    const moreSelectors = request.selectors.filter(sel => sel.selector_type === 'more')
    const result = []
    let cnt =0
    for(let productPage of request.productPages) {
        cnt++
        let prices = []
        let sellers = []
        console.log("starting for ", cnt)
        try {
            await browser.url(request.marketplace.homepage + productPage.path)
            let isPaginatedByMore = moreSelectors.length > 0 
            if(isPaginatedByMore){
                let moreBtn = await browser.$(moreSelectors[0].value)
                while((await moreBtn.isExisting())){
                    await moreBtn.click() 
                }
            }
            do {
                const priceDivs = await getElements(browser, priceSelector.value)
                for(let priceDiv of priceDivs){
                    let price = await priceDiv.getText()
                    console.log(price)
                    if(price.includes(' TL')){
                        prices.push(price)
                    }
                }
                

                const sellerDivs = await getElements(browser, sellerSelector.value)
                for(let sellerDiv of sellerDivs){
                    let seller = await sellerDiv.getText()
                    console.log(seller)
                    sellers.push(seller)
                }
                

                if(!isPaginatedByMore && nextSelectors.length > 0){
                    const nextBtn = await browser.$(nextSelectors[0].value)
                    if((await nextBtn.isExisting())){
                        await nextBtn.click()
                    } else {
                        isPaginatedByMore = true
                    }
                }else{
                    isPaginatedByMore = true
                }
            } while (!isPaginatedByMore)
            
        } catch (error) {
            console.error(error)
        }    
        result.push({prices, sellers})
    }
    await browser.deleteSession()
    return result
}
