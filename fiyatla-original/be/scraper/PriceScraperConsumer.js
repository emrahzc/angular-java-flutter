
const { Consumer } = require('redis-smq');
const { getSellerId } = require('../service/sellerService');
const { query } = require('../util/mysql');
const { scrapePricePage } = require('./priceScraper');

const maxProductPages = 100

const scrapePrices = async (jobId) => {
    await query('update job set status= "STARTED" where id = ?', [jobId])

    const job = (await query('SELECT * from job where id = ?', [jobId]))[0]
    const schedule = (await query('SELECT * from schedule where id = ?', [job.schedule_id]))[0]
    try {
        const marketplace = (await query('select * from marketplace where id = ?', [schedule.marketplace_id]))[0]
        const category = (await query('select * from category where id = ?', [schedule.category_id]))[0]
        const lastId = (job.last_product_page_id?job.last_product_page_id:-1)
        const productPages = await query('select * from product_page where parent_id = (select id from product_list_page where marketplace_id = ? and category_id = ? ) ' 
            + 'and (? =-1 or id > ?)', 
            [marketplace.id, category.id, lastId, lastId])
        const hasTooManyPages = productPages.length > maxProductPages
        const limitedProductPages = hasTooManyPages ? productPages.slice(0, maxProductPages) : productPages
        const selectorGroup = (await query('select * from selector_group where type = "PRODUCT" and category_id = ? and marketplace_id = ?', 
            [category.id, marketplace.id]))[0]
        const selectors = await query('select * from selector where group_id = ? ', [selectorGroup.id])
        
        const results = await scrapePricePage({limitedProductPages, marketplace, selectors})
        let i =0;
        for(let productPage of limitedProductPages){
            const sellerIds = await Promise.all(results[i].sellers.map(async sellerName => {
               return await getSellerId(sellerName)
            }))
            const records = results[i].prices.map((price, index) => {
                return [productPage.product_id, parseFloat(price.replace(/[^0-9,-]+/g,"").replace(',', '.')), marketplace.id, sellerIds[index]]
            })
            console.log("records:", records)
            if(!records || records.length==0 ){
                continue
            }
            await query('insert into price (product_id, price, marketplace_id, seller_id) values ?', 
                [records])
            i++
        }
        
        const lastProductPageId = hasTooManyPages ? productPages[maxProductPages.length] : null
        await query('update job set status= "FINISHED", last_product_page_id=? where id = ?', [lastProductPageId, jobId])
    } catch (error) {
        console.error(error)
        await query('update job set status= "FAILED" where id = ?', [jobId])
    }
} 

class PriceScraperConsumer extends Consumer {
    static queueName = 'price_scrape_queue'

    consume(message, cb) {
        console.log("started consuming")
        if(message.jobId){
            scrapePrices(message.jobId)
        }
        console.log("finished consuming")
        
        cb()
    }
}

module.exports = { PriceScraperConsumer }