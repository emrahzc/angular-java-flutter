
const { Consumer } = require('redis-smq');
const { query } = require('../util/mysql');
const { scrapeUrlPage } = require('./urlScraper');

const scrapeUrls = async (jobId) => {
    await query('update job set status= "STARTED" where id = ?', [jobId])
    console.log('update job set status= "STARTED" where id = ? ', jobId)
    const job = (await query('SELECT * from job where id = ?', [jobId]))[0]
    const schedule = (await query('SELECT * from schedule where id = ?', [job.schedule_id]))[0]
    try {
        const marketplace = (await query('select * from marketplace where id = ?', [schedule.marketplace_id]))[0]
        const category = (await query('select * from category where id = ?', [schedule.category_id]))[0]
        const listPage = (await query('select * from product_list_page where marketplace_id = ? and category_id = ?', [marketplace.id, category.id]))[0]
        const selectorGroup = (await query('select * from selector_group where type = "list" and category_id = ? and marketplace_id = ?', [category.id, marketplace.id]))[0]
        const selectors = await query('select * from selector where group_id = ? ', [selectorGroup.id])
        const productPages = await scrapeUrlPage({page: marketplace.homepage + "/" + listPage.path, selectors})
        if(!productPages || productPages.length == 0){
            await query('update job set status= "FAILED" where id = ?', [jobId])
            return
        }                
        const products = await query('select * from product where name in (?) ', [productPages.map(pp => pp.name)]) 
        await query('delete from product_page where parent_id = ?', [listPage.id])
        console.log("products", products.length)
        for(let productPage of productPages) {
            let productId = products.find(product => product.name === productPage.name)?.id;
            if(!productId){
                const createProduct = await query('insert into product (name, fullname, category_id) values(?,?,?)', [productPage.name, productPage.name, listPage.category_id])
                productId = createProduct.insertId
            }
            const url = new URL(productPage.href)
            const result = await query('insert into product_page (path, parent_id, product_id) values(?, ?, ?)', [url.pathname, listPage.id, productId])
            console.log("inserted product page with id:", result.insertId)
        }
        await query('update job set status= "FINISHED" where id = ?', [jobId])
    } catch (error) {
        console.error(error)
        await query('update job set status= "FAILED" where id = ?', [jobId])
    }
} 

class UrlScraperConsumer extends Consumer {
    static queueName = 'url_scrape_queue'

    consume(message, cb) {
        console.log("started consuming")
        if(message.jobId){
            scrapeUrls(message.jobId)
        }
        console.log("finished consuming")

        cb()
    }
}

module.exports = { UrlScraperConsumer }