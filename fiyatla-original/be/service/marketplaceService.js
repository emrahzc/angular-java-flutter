const { query } = require("../util/mysql");

async function createMarketplace(urlAddress){
	const url = new URL(urlAddress)
	const hostname = url.hostname.replace("www", '');
	const marketplaceName = hostname.substring(0, hostname.indexOf('.'));
	console.log(url.hostname, hostname, marketplaceName);
	const result = await query('insert into marketplace (name, homepage) values(?, ?)',[marketplaceName , url.hostname]);
	return result.insertId;
}

async function getMarketplaceId(url){
	const rows = await query('SELECT * from marketplace where homepage = ?', [url]);
	if(rows.length == 0){
		console.log("marketplace creating new")
		return await createMarketplace(url); 				
	}
	console.log("marketplace already exists ", rows, rows[0].id)
	const result = results=JSON.parse(JSON.stringify(rows))
	console.log("returning:", result[0].id)
	console.log("actual returning", rows[0].id);
	return rows[0].id;
}
module.exports  = {
	getMarketplaceId
};