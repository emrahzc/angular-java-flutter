const { query } = require("../util/mysql");
async function createCategory(name){
	const result = await query('insert into category (name) values(?)',[name]);
	return result.insertId;
}

async function getCategoryId(name){
	const rows = await query('SELECT * from category where lower(name) = lower(?)', name);
	if(rows.length == 0){
		console.log("cat creating new")
		return await createCategory(name);
	}
	console.log("cat already exists ")

	return rows[0].id;
}
module.exports  = {
	createCategory,
	getCategoryId
};