const { query } = require("../util/mysql");

async function createProduct(productName){
	const result = await query('insert into product (name, fullname, category_id) values(?, ?, ?)',[productName, productName, 1]);
	return result.insertId;
}

async function getProductId(productName){
	const rows = await query('SELECT * from product where name = ?', [productName]);
	if(rows.length == 0){
		return await createProduct(productName);
	}
	console.log(rows);
	return rows[0].id;
}
module.exports  = {
	getProductId,
	createProduct
};