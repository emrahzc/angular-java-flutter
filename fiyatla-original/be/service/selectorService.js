const { query } = require("../util/mysql");

module.exports  = {
	createSelector : async (groupId, selector_type, value) => {
		const result = await query('insert into selector (group_id, selector_type, value) values(?, ?, ?)', [groupId, selector_type, value]);
		return result.insertId; 
	},
	createSelectorGroup: async (type, marketplaceId, categoryId) => {
		const result = await query('insert into selector_group (type, marketplace_id, category_id) values(?, ?, ?)', [type, marketplaceId, categoryId]);
		return result.insertId; 
	},
	deleteOldSelectors: async (type, marketplaceId, categoryId) => {
		await query('delete from selector where group_id in (select id from selector_group where type = ? and category_id = ? and marketplace_id = ?)', [type, categoryId, marketplaceId]);
		await query('delete from selector_group where type = ? and category_id = ? and marketplace_id = ?', [type, categoryId, marketplaceId]);
	}
};