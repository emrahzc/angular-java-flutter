const { query } = require("../util/mysql");

async function createSeller(name){
	const result = await query('insert into seller (name) values(?)',[name]);
	return result.insertId;
}

async function getSellerId(name){
	const rows = await query('SELECT * from seller where name = ?', [name]);
	if(rows.length == 0){
		console.log("seller creating new")
		return await createSeller(name); 				
	}
	return rows[0].id;
}
module.exports  = {
	getSellerId
};