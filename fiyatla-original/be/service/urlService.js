const { query } = require("../util/mysql");
const { getCategoryId } = require("./categoryService");
const { getMarketplaceId } = require("./marketplaceService");

async function createProductListPage(path, categoryId, marketplaceId) {
	const result = await query('insert into product_list_page (path, category_id, marketplace_id) values(?, ?, ?)',[path, categoryId, marketplaceId]);
	return result.insertId;
}

async function createProductPage(path, parentId, productId) {
	const result = await query('insert into product_page (path, parent_id, product_id) values(?, ?, ?)', [path, parentId, productId]);
	return result.insertId;
}

module.exports  = {
	getProductListId: async (urlStr, categoryName) => {
		console.log("url", urlStr)
		let url = new URL(urlStr);
		let categoryId = await getCategoryId(categoryName);
		let marketplaceId = await getMarketplaceId(url.origin);
		const rows = await query('SELECT * from product_list_page where category_id = ? and marketplace_id = ?', [categoryId, marketplaceId]);
		if(rows.length == 0){
			return await createProductListPage(url.pathname, categoryId, marketplaceId);
		}
		return rows[0].id;
	},
	createProductListPage,
	getProductPageId: async (urlStr, categoryName) => {
		let url = new URL(urlStr);
		const rows = await query('SELECT * from product_page where path = ? and marketplace_id = ?)', [url.pathname, marketplaceId]);
		if(rows.length == 0){
			console.error('cant find productPage!')
			return -1
		}
		return rows[0].id;
	},
	createProductPage
};