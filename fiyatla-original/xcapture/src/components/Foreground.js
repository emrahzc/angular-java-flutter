import React from 'react';

class SelectionDiv extends React.Component{

    styles = {
        main: {
            float: 'right',
            marginTop: '30px',
            backgroundColor: 'darkblue',
            borderRadius: '5px',
            padding: '5px',
            color: 'white',
            width: '300px'
        },
        selectors: {

        },
        saveSelector: {
            background: 'lightgreen',
            padding: '5px',
            borderRadius: '5px'
        },
        clearSelector: {
            background: 'red',
            padding: '5px',
            borderRadius: '5px',
            marginLeft: '5px',
            marginRight: '5px'
        },
        selectorType: {
            width: '50px',
            borderRight: '1px solid lightgrey'
        },
        selectorTokens: {
            textOverflow: 'ellipsis',
            width: '150px',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            display: 'block'

        },
        tableheader:{
            borderBottom: '1px solid lightgrey'
        },
        bottomBar: {
            margin: '5px',
            marginTop: '25px'
        },
        bigButton: {
            padding:'5px',
            margin: '5px',
            float: 'right'
        }
    };

    constructor(props){
        super(props);
        this.state = {
            categoryOptions: ['smartphone', 'tablet', 'laptop'],
            pageTypeOptions: ['urls', 'prices'],
            selectorOptions: ['image', 'url', 'price', 'seller', 'next', 'more']
        }
    }

    render(){
        return <div style={this.styles.main}>
            <div style={{width:"100%", textAlign: 'right'}}>
                page type: <select name='selectedPageTypeOption' onChange={this.props.changeSelectorType} defaultValue={this.props.selectedPageTypeOption}>
                    {this.state.pageTypeOptions.map(i =>   <option value={i} key={i} >{i}</option>)}
                </select>
            <br />
            category: <select name='selectedCategory' onChange={this.props.changeSelectorType} defaultValue={this.props.selectedCategory}>
                    {this.state.categoryOptions.map(i =>   <option value={i} key={i} >{i}</option>)}
                </select>

            </div>
            {(this.props.recordedSelectors.length> 0) && <table style = {{width: '100%'}}>
                <thead style={this.styles.tableheader}>
                    <tr>
                        <th>type</th>
                        <th style={this.styles.selectorTokens}>tokens</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {this.props.recordedSelectors.map((selector, i) =>
                    <tr style = {{marginBottom: '5px'}} key={i} >
                        <td style={this.styles.selectorType}>{selector.type}</td>
                        <td style={this.styles.selectorTokens}>{selector.tokens.join('|')}</td>
                        <td><button style={this.styles.clearSelector} onClick = {() => this.props.deleteSelector(i)}>delete</button></td>
                    </tr>    
                )}
                </tbody>
            </table>}
            
                <div style={{marginTop:'5px'}} >
                    <h5 style={{borderBottom: '1px solid lightgrey'}}>current selection</h5>
                    selector type: <select name='currentSelectorType' onChange={this.props.changeSelectorType} defaultValue={this.props.currentSelectorType}>
                        {this.state.selectorOptions.map(i =>   <option value={i} key={i} >{i}</option>)}
                    </select>                    
                    <button style={this.styles.clearSelector} onClick={this.props.clearSelector}>clear</button>
                    <button style={this.styles.saveSelector} onClick={this.props.saveSelector}>save selector</button>
                </div>
            
            {<div style={this.styles.bottomBar}>
                <h5 style={{borderBottom: '1px solid lightgrey'}}>page actions</h5>
                <button style={this.styles.clearSelector} onClick={this.props.close}>close</button>
                {this.props.recordedSelectors.length> 0 && <button style={Object.assign({}, this.styles.saveSelector, this.styles.bigButton)} onClick={this.props.savePage}>save page</button> }
                {this.props.recordedSelectors.length> 0 && <button style={Object.assign({}, this.styles.clearSelector, this.styles.bigButton)} onClick={this.props.clearSelector}>clear all</button>}
            </div>}
        </div>;
    }
}
const colors = ['Tomato','Orange','DodgerBlue','MediumSeaGreen','Gray','SlateBlue','Violet','LightGray']; 

function getUniqueSelector(node) {
	let selector = [];
    console.log(node)
	while (node.parentElement) {
        console.log(node)
	  const siblings = Array.from(node.parentElement.children).filter(
		e => e.tagName === node.tagName
	  );
	selector.unshift(siblings.indexOf(node)+1);
	selector.unshift(node.tagName.toLowerCase());  
	node = node.parentElement;
	}
	selector.unshift("html");
	return selector;
}

function generateSelector(arr){
	let result = arr[0];
	for(let i=1;i<arr.length; i++){
		if(typeof arr[i] === "number") result += ":nth-of-type(" + arr[i] + ")";
		else result += " > " + arr[i];  
	}
	return result;
}

class Foreground extends React.Component {
    state = {
        isStarted: false, 
        showCover: false, 
        recordedSelectors: [], 
        currentSelector:[], 
        newSelector: false,
        currentElements: [],
        selectedPageTypeOption: 'urls',
        selectedCategory: 'smartphone',
        currentSelectorType: 'image'
    };
    styles = {
        main: {
            opacity:'0.8',
            position:'fixed',
            width:'100%',
            height:'100%',
            top:'0px',
            left:'0px',
            zIndex:'1000',
            backgroundColor:'#cccccc75'
        }
    }
    constructor(props){
        super(props);
        this.savePage  = this.savePage.bind(this)
        this.getRightClickedElement = this.getRightClickedElement.bind(this)
        this.close = this.close.bind(this)
        this.clearSelector = this.clearSelector.bind(this)
        this.saveSelector = this.saveSelector.bind(this)
        this.changeSelectorType = this.changeSelectorType.bind(this)
        this.deleteSelector = this.deleteSelector.bind(this)

        chrome.runtime.onMessage.addListener( (request, _, respond) => {
            this.setState({
                isStarted:true, 
                showCover: true,
                selectors: [],
            });
            if(request.type==="record"){
                respond(window.location.href)
            }else if(request.type==="stop"){
                respond({
                        data:{selectors: this.state.recordedSelectors, 
                        url: window.location.href, 
                        selectedCategory: this.state.selectedCategory,
                        pageType: this.state.selectedPageTypeOption
                    }});
                this.setState({isStarted:false});
                alert("recording saved!");
                this.close();
            }
        })
    
    }
    checkSelectors(lastSelector, last2Selector){
        console.log("checking selectors for ", this.state.currentSelectorType, generateSelector(lastSelector), last2Selector);
        if(!last2Selector){
            if(this.state.currentSelectorType === "next" || this.state.currentSelectorType === "more"){
                this.setState({currentSelector: generateSelector(lastSelector), currentElements: [lastSelector]});
            }
            console.log("nope");
            return;
        }else{
            this.setState({newSelector: true});
        }
        let tokens = [];
        console.log("copying", lastSelector, last2Selector);
        let arr1 = [...lastSelector];
        let arr2 = [...last2Selector];
    
        let idx2 =1;
        let idx1 =1;
        let commonStr = "html";
        while(idx1 < arr1.length && idx2 < arr2.length){
            if(arr1[idx1] === arr2[idx2]){
                commonStr += (typeof arr2[idx2] !== "number") ? (" > " + arr2[idx2]) : (":nth-of-type(" +arr2[idx2]+ ")");  
            }else{
                tokens.push(commonStr);
                commonStr = "";
                if(arr2.length > idx2+1 && arr1[idx1] === arr2[idx2+1]){
                    idx2++;
                }else if(arr1.length > idx1+1 && arr2[idx2] === arr1[idx1+1]){
                    idx1++;
                }else if(arr1.length > idx1+1 && arr2.length > idx2+1 && arr1[idx1+1] !== arr2[idx2+1]){
                    console.log("detected not equals", arr1, arr2, idx1, idx2)
                    return;
                }
            }
            idx1++;
            idx2++;
        }
        if(commonStr.length > 0){
            tokens.push(commonStr);
        }
        console.log("tokens:", tokens)
        let populatedElements = this.populateSelectors([...tokens])
        if(populatedElements.length < 3){
            let els = document.querySelector(generateSelector(last2Selector));
            if(!els.className){
                return;
            }
            populatedElements = this.populateSelectorsFromClassName(els.tagName + "." +els.className);
            this.setBorder(populatedElements, this.state.recordedSelectors.length);
            this.setState({currentSelector: [els.tagName + "." +els.className], currentElements: populatedElements});
            console.log(els.tagName, els.className)
        }else{
            this.setBorder(populatedElements, this.state.recordedSelectors.length);
            this.setState({currentSelector: tokens, currentElements: populatedElements});
        }
    }
    setBorder(elements, colorIndex){
        if(Array. isArray(elements)){
            elements.forEach(el => 
                document.querySelector(el).style.border = (colorIndex > -1 ? "2px solid " + colors[colorIndex] : "none")
                );    
        }else if(document.querySelector(elements)){
            document.querySelector(elements).style.border = (colorIndex > -1 ? "2px solid " + colors[colorIndex] : "none")
        }
    }
    populateSelectorsFromClassName(selector){
        return [...document.querySelectorAll(selector)].map(element => generateSelector(getUniqueSelector(element)));
    }
    populateSelectors(currentTokens){
        if(currentTokens.length == 1){
            return currentTokens[0];
        }
        let idx =1;
        let first = currentTokens.shift();
        let second = currentTokens.shift();
        let result = [];
        while(idx < 100){
            if(document.querySelector(first + ":nth-of-type(" + idx + ")" + second)){
                currentTokens.unshift(first + ":nth-of-type(" + idx + ")" + second)
                result = result.concat(this.populateSelectors([...currentTokens]));
                currentTokens.shift()
            }
            idx++;
        }
        return result;
    }

    componentDidUpdate() {
        if(this.state.isStarted && !this.state.showCover){ // after getRightClickedElement
            let element = document.elementFromPoint(this.state.location.x, this.state.location.y)
            console.log(element);
            const uniqueSelector = getUniqueSelector(element);
            const uniqueSelectorStr = generateSelector(uniqueSelector);
            console.log(uniqueSelectorStr);
            let newSelectors = [...this.state.selectors];
            let lastSelector = this.state.lastSelector;
            let last2Selector = this.state.last2Selector;
            if(this.state.selectors.includes(uniqueSelectorStr)){
                newSelectors = newSelectors.splice(newSelectors.indexOf(uniqueSelectorStr), 1);
                document.querySelector(uniqueSelectorStr).style.border = "none";
                console.log("remove already existing");
            }else{
                last2Selector = lastSelector;
                lastSelector = uniqueSelector;
                element.style.border = "2px solid " + colors[this.state.recordedSelectors.length];
                this.checkSelectors(lastSelector, last2Selector);
            }
            console.log("set state at the end of didupdate");

            this.setState({isStarted: true, showCover: true, selectors: newSelectors, lastSelector, last2Selector});
        }
    }

    getRightClickedElement(event){
        event.preventDefault()
        event.stopPropagation()
        let st = {isStarted: true, showCover: false, location: {x: event.clientX, y: event.clientY}};
        this.setState(st)
    }
    saveSelector(){
        console.log("save selector!");
        this.setState({
                newSelector: false, 
                recordedSelectors: [...this.state.recordedSelectors, {elements: this.state.currentElements, type: this.state.currentSelectorType, tokens: this.state.currentSelector}], 
                currentSelector: [],
                currentSelectorType: 'image',
                currentElements: []
        });
    }
    deleteSelector(i){
        console.log("delete selector!");
        this.setBorder(this.state.currentElements, -1);
        console.log("delete:", i, this.state.recordedSelectors)
        let recordedSelectors = [...this.state.recordedSelectors];
        this.setBorder(recordedSelectors[i].elements, -1);
        console.log(i, recordedSelectors)
        recordedSelectors.splice(i, 1);
        this.setState({recordedSelectors});
    }
    close(){
        this.state.recordedSelectors.forEach(record => this.setBorder(record.elements, -1));
        this.setState({isStarted: false, recordedSelectors: [], currentSelector: [], newSelector: false});
    }
    clearSelector(){
        console.log("clear selector!");
        this.setBorder(this.state.currentElements, -1);
        this.setState({
                newSelector: false, 
                currentSelector: [],
                currentSelectorType: 'image',
                currentElements: []
        });
    }
    changeSelectorType(event){
        this.setState({[event.target.name]: event.target.value});
    }
    savePage() {
        chrome.runtime.sendMessage({type:"stopRecording", data:{
            selectors: this.state.recordedSelectors, 
            url: window.location.href, 
            selectedCategory: this.state.selectedCategory,
            pageType: this.state.selectedPageTypeOption}});
    }
    render() {
        return (
            this.state.isStarted && this.state.showCover && <div style={this.styles.main} onContextMenuCapture= {this.getRightClickedElement}>
                <h1>recording - right click elements to scrape</h1>
                <SelectionDiv 
                recordedSelectors={this.state.recordedSelectors}
                 
                selectors = {this.state.selectors} 
                newSelector= {this.state.newSelector}
                currentSelectorType = {this.state.currentSelectorType}
                changeSelectorType= {this.changeSelectorType}
                saveSelector = {this.saveSelector}
                clearSelector = {this.clearSelector}
                savePage = {this.savePage}
                deleteSelector = {this.deleteSelector}
                selectedPageTypeOption = {this.state.selectedPageTypeOption}
                close = {this.close}
                />
            </div>
        )
    }
}


export default Foreground;