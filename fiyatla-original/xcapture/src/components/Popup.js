import React, { useState } from 'react';


const styles = {
    main: {
        width: '300px',
        height: '200px',
        textAlign: 'center'
    },
    startbtn: {
 
    },
    stopbtn: {
 
    },
    guide: {
 
    }
}




function Popup() {
    const [isStarted, setStarted] = useState(false);
    const [isInit, setInit] = useState(true); //search for loading page flag on SO

    chrome.runtime.onMessage.addListener(
        function(request, sender, sendResponse) {
            console.log("got message", request)
            if (request.type === "pageStarted") {
                setInit(true);
            }
        }
    );
    function startRecording(e)  {
        e.preventDefault();
        setStarted(true);
        console.log("");
        chrome.runtime.sendMessage({type:"start"})
    }
    
    function stopRecording(e)  {
        e.preventDefault();
        setStarted(false);
        chrome.runtime.sendMessage({type:"stop"})
    }
    
    return (
        <div style={styles.main}>
            { isInit ? (!isStarted ? 
                <button id="startbtn" style={styles.startbtn} onClick={startRecording}>start recording</button>
                :
                <button id="stopbtn" style={styles.stopbtn} onClick={stopRecording}>stop recording</button>
             ) : "please wait"}
            { isStarted && <h4 style={styles.guide}>right click elements to scrape</h4> }
        </div>
    )
}

export default Popup;