import React from 'react';
import { render } from 'react-dom';

import Foreground from './components/Foreground.js';
chrome.runtime.sendMessage({type:"pageStarted"});

render(<Foreground />, document.querySelector('#foreground'));