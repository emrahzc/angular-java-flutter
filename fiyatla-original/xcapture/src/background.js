/**
 *   background.js
 * 
 *   hosts the logic in the background mediating between the popup and page script.
 *   
 */
 let currentEvent = -1
 let state
 let currentSite 
 let currentTab
 let reloaded
 let data = initData('inactive', -1)

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (changeInfo.status === 'complete' && tab.url.includes('http')) {
        chrome.tabs.executeScript(tabId, { file: './inject_script.js' }, function () {
            chrome.tabs.executeScript(tabId, { file: './foreground.bundle.js' }, function () {
                console.log('INJECTED AND EXECUTED');
            });
        });
    }
});
 
 chrome.runtime.onMessage.addListener(function(request, sender, respond){
 
    if(sender.url.endsWith('popup.html')){
        switch(request.type) {
            case "start":
                startRecording()
                break
            case "stop":
                stopRecording(request.data)
                break
            default:
                console.error("unknown request from popup")
                console.error(request)
        }    
    }else{
        switch (request.type) {
            case "stopRecording":
                console.log("stopRecording", request)
                stopRecording(request.data)
                break
                case "pageStarted":
                    console.log("stopRecording", request)
                    initPage()
                    break    
            default:
                console.error("unknown request from page")
                console.error(request)
        }
    }
 })
 function initPage() {
    chrome.runtime.sendMessage({
        type: "init"
    });
 }
 function stopRecording(data) {
 
    /** 
     chrome.tabs.sendMessage(currentTab, { type:"stop" }, function(response){
         console.log("response", response);
         fetch("http://localhost/api/selector", { //set dynamic
             method: 'POST',
             mode: 'no-cors',
             cache: 'no-cache',
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded',
             },
             body: response.data
         }).then(function(res) {
             console.log("response from be:", res)
         });
     });
     chrome.browserAction.setIcon({ path: "assets/play.png" });
     
     state = "inactive";
     */
    const body = {
        'Authorization': 'Basic ' + btoa("fiyatla-extension|f1y@atla"),
        'pageType': data.pageType,
        'selectors': data.selectors.map(selector => {
            return {
                'type': selector.type,
                'selector': selector.tokens.join(' ') 
            }
        }),
        'category': data.selectedCategory,
        'url': data.url,
        'type': data.pageType == "urls" ? "list" : "product"
    }
    console.log("body", body)
     fetch("http://172.26.255.255:3000/api/selector/extension", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify(body)
    }).then(function(res) {
        console.log("response from be:", res)
        chrome.tabs.sendMessage(currentTab, { type:"stopped" })
    });
 }
 
 function startRecording() {
     console.log("starting to record")
     chrome.browserAction.setIcon({ path: "assets/record.png" });	
     chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
         chrome.tabs.sendMessage(tabs[0].id, { type:"record" }, function (response) {
             currentSite = response;
             console.log("response from page.start:"+response)
             data = initData('record', tabs[0].id)
         });
     });
 }
 
 function initData(_state, _currenTab){
 
     state = _state
     reloaded = true
     currentTab = _currenTab
     return {events:[], finalSite:''}
 }