import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'pageFunctions/enter_correct_username_password.dart';
import 'pageFunctions/enter_username_password.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('Should show price list', (WidgetTester tester) async {
    await enterCorrectUserNamePassword(tester);
  });

  testWidgets('Should show forward to home page when logged in',
      (WidgetTester tester) async {
    await enterUserNamePassword(tester, "username", "wrongFassword");
    expect(find.byKey(Key('failed popup')), findsOneWidget);
  });
}
