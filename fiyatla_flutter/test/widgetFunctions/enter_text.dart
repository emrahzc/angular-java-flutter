import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

enterTextToField(WidgetTester tester, String key, String text) async {
  await tester.enterText(find.byKey(Key(key)), text);
}
