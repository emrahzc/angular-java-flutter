import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

tapWidget(WidgetTester tester, String key) async {
  var widget = find.byKey(Key(key));
  await tester.tap(widget);
}
