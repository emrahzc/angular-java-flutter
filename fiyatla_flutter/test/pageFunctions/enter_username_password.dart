import 'package:flutter_test/flutter_test.dart';

import '../widgetFunctions/enter_text.dart';
import '../widgetFunctions/tap_widget.dart';

enterUserNamePassword(
    WidgetTester tester, String username, String password) async {
  await enterTextToField(tester, 'usernameTextId', username);
  await enterTextToField(tester, 'passwordField', password);
  await tapWidget(tester, 'mySubmitButton');
}
