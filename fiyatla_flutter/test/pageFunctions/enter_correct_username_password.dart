import 'package:flutter_test/flutter_test.dart';

import '../widgetFunctions/enter_text.dart';
import '../widgetFunctions/tap_widget.dart';

enterCorrectUserNamePassword(WidgetTester tester) async {
  await enterTextToField(tester, 'usernameTextId', "CORRECT_PASSWORD");
  await enterTextToField(tester, 'passwordField', 'password');
  await tapWidget(tester, 'mySubmitButton');
}
