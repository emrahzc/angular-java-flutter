// export interface constants(){}

//Log-in test constants

const String CORRECT_USERNAME_FOR_LOG_IN = "test_username_l";
const String CORRECT_PASSWORD_FOR_LOG_IN = "12345aA.";

//Sign-up test constants

//  Correct
const String CORRECT_USERNAME_FOR_SIGN_UP = "test_username_s";
const String CORRECT_PASSWORD_FOR_SIGN_UP = "12345aA.";

//  Wrong Username
const String WRONG_USERNAME_FOR_SIGN_UP = "asd.";

//  Wrong Password without number
const String WRONG_PASSWORD_FOR_SIGN_UP_NO_NUM = "Aa.bbbbb";
const String WRONG_PASSWORD_FOR_SIGN_UP_NO_SPEC = "Aa1bbbbb";
const String WRONG_PASSWORD_FOR_SIGN_UP_NO_CAP = "1a.bbbbb";
const String WRONG_PASSWORD_FOR_SIGN_UP_NO_SMALL = "A1.23456";
const String WRONG_PASSWORD_FOR_SIGN_UP_NO_LENGTH = "Aa1.234";
