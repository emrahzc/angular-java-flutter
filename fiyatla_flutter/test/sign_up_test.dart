import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'constants.dart';
import 'pageFunctions/enter_username_password.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  // Test sign-up with correct username and password
  testWidgets('Should show forward to login page when signed up',
      (WidgetTester tester) async {
    await enterUserNamePassword(
        tester, CORRECT_USERNAME_FOR_SIGN_UP, CORRECT_PASSWORD_FOR_SIGN_UP);
    expect(find.byKey(Key('failed popup')), findsOneWidget);
  });

  // Test sign-up with wrong username and password

  // Wrong Username
  testWidgets('Should show forward to login page when signed up',
      (WidgetTester tester) async {
    await enterUserNamePassword(
        tester, WRONG_USERNAME_FOR_SIGN_UP, CORRECT_PASSWORD_FOR_SIGN_UP);
    expect(find.byKey(Key('failed popup')), findsOneWidget);
  });

  // Wrong Password without capital letter
  testWidgets('Should show forward to login page when signed up',
      (WidgetTester tester) async {
    await enterUserNamePassword(tester, CORRECT_USERNAME_FOR_SIGN_UP,
        WRONG_PASSWORD_FOR_SIGN_UP_NO_CAP);
    expect(find.byKey(Key('failed popup')), findsOneWidget);
  });

  // Wrong Password without small letter
  testWidgets('Should show forward to login page when signed up',
      (WidgetTester tester) async {
    await enterUserNamePassword(tester, CORRECT_USERNAME_FOR_SIGN_UP,
        WRONG_PASSWORD_FOR_SIGN_UP_NO_SMALL);
    expect(find.byKey(Key('failed popup')), findsOneWidget);
  });

  // Wrong Password without number
  testWidgets('Should show forward to login page when signed up',
      (WidgetTester tester) async {
    await enterUserNamePassword(tester, CORRECT_USERNAME_FOR_SIGN_UP,
        WRONG_PASSWORD_FOR_SIGN_UP_NO_NUM);
    expect(find.byKey(Key('failed popup')), findsOneWidget);
  });

  // Wrong Password without special character
  testWidgets('Should show forward to login page when signed up',
      (WidgetTester tester) async {
    await enterUserNamePassword(tester, CORRECT_USERNAME_FOR_SIGN_UP,
        WRONG_PASSWORD_FOR_SIGN_UP_NO_SPEC);
    expect(find.byKey(Key('failed popup')), findsOneWidget);
  });

  // Wrong Password with small length
  testWidgets('Should show forward to login page when signed up',
      (WidgetTester tester) async {
    await enterUserNamePassword(tester, CORRECT_USERNAME_FOR_SIGN_UP,
        WRONG_PASSWORD_FOR_SIGN_UP_NO_LENGTH);
    expect(find.byKey(Key('failed popup')), findsOneWidget);
  });
}
