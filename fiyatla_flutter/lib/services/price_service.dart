import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

import '../model/price.dart';

Future<List<Price>> fetchPrices() async {
  final response = await http.get(Uri.http("192.168.1.19:8080", "/price"));
  if (response.statusCode == 200) {
    return (jsonDecode(response.body) as List<dynamic>)
        .map((priceMap) => Price.fromJson(priceMap))
        .toList();
  } else {
    throw Exception('Failed to load prices');
  }
}

Future<Price> createPrice(String name) async {
  final response = await http.post(
    Uri.http("192.168.1.19:8080", "/price"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'name': name,
    }),
  );

  if (response.statusCode == 201 || response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return Price.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to create price.');
    //log(response.statusCode.toString());
  }
}

void deletePrice(String id) async {
  //og(id);
  final response = await http.delete(
    Uri.http("192.168.1.19:8080", "/price/$id"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );

  if (response.statusCode == 200 || response.statusCode == 204) {
    // If the server did return a 200 OK response,
    // then parse the JSON. After deleting,
    // you'll get an empty JSON `{}` response.
    // Don't return `null`, otherwise `snapshot.hasData`
    // will always return false on `FutureBuilder`.
    log(response.statusCode.toString());
  } else {
    // If the server did not return a "200 OK response",
    // then throw an exception.
    log(response.statusCode.toString());
    throw Exception("failed to delete price");
  }
}

Future<Price> updatePrice(String name) async {
  final response = await http.put(
    Uri.http("192.168.1.19:8080", "/price"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'name': name,
    }),
  );

  if (response.statusCode == 201 || response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return Price.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    log(response.statusCode.toString());
    return Price.fromJson(jsonDecode(response.body));
    //log(response.statusCode.toString());
  }
}
