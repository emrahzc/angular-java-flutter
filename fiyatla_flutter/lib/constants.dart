import 'package:flutter/material.dart';

const String LENGTHPATTERN = '.{8,}';
const String CAPITALLETTERPATTERN = r'^(?=.*?[A-Z])';
const String SMALLLETTERPATTERN = r'^(?=.*?[a-z])';
const String NUMBERPATTERN = r'^(?=.*?[0-9])';
const String SPECIALCHARPATTERN = r'^(?=.*?[!@#\$&.,*~£])';

ListTile buildUserPageDrawerListTile(
    {String text, BuildContext context, Widget page}) {
  return ListTile(
    title: Text(text),
    onTap: () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => page,
        ),
      );
    },
  );
}
