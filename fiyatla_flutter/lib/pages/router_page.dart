import 'package:fiyatla_flutter/pages/aut_router_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'login_page.dart';

class RouterPage extends StatefulWidget {
  RouterPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RouterPageState createState() => _RouterPageState();
}

class _RouterPageState extends State<RouterPage> {
  int index = 0;
  int argv1 = 0;
  int argv2 = 1;

  void passArgsToParent(arg1, arg2) {
    setState(() {
      this.argv1 = argv1;
      this.argv2 = argv2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (context) => RouterPage()),
        //ChangeNotifierProvider(create: create)
      ],
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Offstage(
              offstage: index != 0,
              child: TickerMode(
                enabled: index == 0,
                child: MaterialApp(
                  home: AutRouterPage(title: 'Aut Router Page'),
                ),
              ),
            ),
            Offstage(
              offstage: index != 1,
              child: TickerMode(
                enabled: index == 1,
                child: MaterialApp(
                  home: LoginPage(title: 'login'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
