import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'home_page.dart';
import 'login_page.dart';
import 'signup_page.dart';

class AutRouterPage extends StatefulWidget {
  AutRouterPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AutRouterPageState createState() => _AutRouterPageState();
}

class _AutRouterPageState extends State<AutRouterPage> {
  int index = 0;
  int argv1 = 0;
  int argv2 = 1;

  void passArgsToParent(arg1, arg2) {
    setState(() {
      this.argv1 = argv1;
      this.argv2 = argv2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Offstage(
            offstage: index != 0,
            child: TickerMode(
              enabled: index == 0,
              child: MaterialApp(
                home: HomePage(title: 'Fiyatla'),
              ),
            ),
          ),
          Offstage(
            offstage: index != 1,
            child: TickerMode(
              enabled: index == 1,
              child: MaterialApp(
                home: LoginPage(title: 'login'),
              ),
            ),
          ),
          Offstage(
            offstage: index != 2,
            child: TickerMode(
              enabled: index == 2,
              child: MaterialApp(
                home: SignUpPage(
                    title: 'SignUp', passArgsToParent: passArgsToParent),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: index,
        onTap: (int idx) {
          setState(() {
            index = idx;
          });
        },
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: FaIcon(FontAwesomeIcons.home),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: FaIcon(FontAwesomeIcons.signInAlt),
            label: "Login",
          ),
          BottomNavigationBarItem(
            icon: FaIcon(FontAwesomeIcons.userPlus),
            label: "Sign Up",
          ),
        ],
      ),
    );
  }
}
