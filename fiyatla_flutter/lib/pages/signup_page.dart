import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../constants.dart';
import '../widgets/password_condition_text_widget.dart';
import '../widgets/password_username_textfield_widget.dart';

class SignUpPage extends StatefulWidget {
  final passArgsToParent;

  SignUpPage({Key key, this.title, this.passArgsToParent}) : super(key: key);

  final String title;

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final usernameTextController = TextEditingController();
  final passwordTextController = TextEditingController();
  final repPasswordTextController = TextEditingController();

  bool lengthValidity = false,
      capitalLetterValidity = false,
      smallLetterValidity = false,
      specialLetterValidity = false,
      numberValidity = false,
      checkIfPassSame = false;

  @override
  void dispose() {
    usernameTextController.dispose();
    passwordTextController.dispose();
    repPasswordTextController.dispose();
    super.dispose();
  }

  bool checkValidity(String pass, String pattern) {
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(pass);
  }

  Function passValidity() {
    return (value) {
      setState(() {
        lengthValidity = checkValidity(value, LENGTHPATTERN);
        capitalLetterValidity = checkValidity(value, CAPITALLETTERPATTERN);
        smallLetterValidity = checkValidity(value, SMALLLETTERPATTERN);
        specialLetterValidity = checkValidity(value, SPECIALCHARPATTERN);
        numberValidity = checkValidity(value, NUMBERPATTERN);
      });
    };
  }

  Function checkRepPass() {
    return (value) {
      setState(() {
        if (passwordTextController.text == repPasswordTextController.text) {
          checkIfPassSame = true;
        }
      });
    };
  }

  bool isPassValid() {
    return numberValidity &&
        specialLetterValidity &&
        smallLetterValidity &&
        capitalLetterValidity &&
        lengthValidity &&
        checkIfPassSame;
  }

  @override
  void initState() {
    super.initState();
    //coursesFuture = fetchCourses();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Container(
          margin: EdgeInsets.only(top: 10),
          width: MediaQuery.of(context).size.width * 0.65,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              PassUserTextField(
                controller: usernameTextController,
                labelText: 'Enter your username',
                onChangedFunction: (string) => {},
                obscureText: false,
              ),
              PassUserTextField(
                controller: passwordTextController,
                labelText: 'Enter your password',
                onChangedFunction: passValidity(),
                obscureText: true,
              ),
              PassUserTextField(
                controller: repPasswordTextController,
                labelText: 'Enter your password again',
                onChangedFunction: checkRepPass(),
                obscureText: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: isPassValid() ? () {} : null,
                    child: Text('Sign Up'),
                  ),
                ],
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ConditionText(
                          txt: 'Password Length', validity: lengthValidity),
                      ConditionText(
                          txt: 'Capital Letter',
                          validity: capitalLetterValidity),
                      ConditionText(
                          txt: 'Small Letter', validity: smallLetterValidity),
                      ConditionText(
                          txt: 'Special Character',
                          validity: specialLetterValidity),
                      ConditionText(txt: 'Number', validity: numberValidity),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
