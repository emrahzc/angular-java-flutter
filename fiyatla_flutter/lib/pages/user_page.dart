import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../constants.dart';
import 'home_page.dart';
import 'signup_page.dart';

class UserPage extends StatefulWidget {
  UserPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('Drawer Header'),
            ),
            buildUserPageDrawerListTile(
                text: 'Job',
                context: context,
                page: SignUpPage(
                  title: 'signup',
                )),
            buildUserPageDrawerListTile(
                text: 'Marketplace',
                context: context,
                page: SignUpPage(
                  title: 'signup',
                )),
            buildUserPageDrawerListTile(
                text: 'Price',
                context: context,
                page: SignUpPage(
                  title: 'signup',
                )),
            buildUserPageDrawerListTile(
                text: 'Product',
                context: context,
                page: SignUpPage(
                  title: 'signup',
                )),
            buildUserPageDrawerListTile(
                text: 'Schedule',
                context: context,
                page: SignUpPage(
                  title: 'signup',
                )),
            buildUserPageDrawerListTile(
                text: 'Seller',
                context: context,
                page: SignUpPage(
                  title: 'signup',
                )),
            buildUserPageDrawerListTile(
                text: 'Log-out',
                context: context,
                page: HomePage(
                  title: 'Home Page',
                )),
          ],
        ),
      ),
      resizeToAvoidBottomInset: false,
      body: Center(),
    );
  }
}
