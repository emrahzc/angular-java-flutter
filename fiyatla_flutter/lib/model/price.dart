import 'package:flutter/widgets.dart';

class Price {
  final int id;
  final String name;

  Price({@required this.id, @required this.name, String category});

  factory Price.fromJson(Map<String, dynamic> json) {
    return Price(id: json['id'], name: json['name'], category: 'school-course');
  }
}
