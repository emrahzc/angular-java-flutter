import 'package:flutter/widgets.dart';

class Seller {
  final int id;
  final String name;

  Seller({@required this.id, @required this.name, String category});

  factory Seller.fromJson(Map<String, dynamic> json) {
    return Seller(
        id: json['id'], name: json['name'], category: 'school-course');
  }
}
