import 'package:flutter/cupertino.dart';

class UserRouterIndex extends ChangeNotifier {
  int user_router_index = 0;

  void updateUserRouterIndex(int new_index) {
    user_router_index = new_index;
    notifyListeners();
  }
}
