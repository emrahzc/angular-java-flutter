import 'package:flutter/cupertino.dart';

class RouterIndex extends ChangeNotifier {
  int aut_router_index = 0;

  void updateRouterIndex(int new_index) {
    aut_router_index = new_index;
    notifyListeners();
  }
}
