import 'package:flutter/widgets.dart';

class Schedule {
  final int id;
  final String name;

  Schedule({@required this.id, @required this.name, String category});

  factory Schedule.fromJson(Map<String, dynamic> json) {
    return Schedule(
        id: json['id'], name: json['name'], category: 'school-course');
  }
}
