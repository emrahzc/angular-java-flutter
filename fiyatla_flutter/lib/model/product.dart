import 'package:flutter/widgets.dart';

class Product {
  final int id;
  final String name;

  Product({@required this.id, @required this.name, String category});

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
        id: json['id'], name: json['name'], category: 'school-course');
  }
}
