import 'package:flutter/widgets.dart';

class Job {
  final int id;
  final String name;

  Job({@required this.id, @required this.name, String category});

  factory Job.fromJson(Map<String, dynamic> json) {
    return Job(id: json['id'], name: json['name'], category: 'school-course');
  }
}
