import 'package:flutter/widgets.dart';

class Marketplace {
  final int id;
  final String name;

  Marketplace({@required this.id, @required this.name, String category});

  factory Marketplace.fromJson(Map<String, dynamic> json) {
    return Marketplace(
        id: json['id'], name: json['name'], category: 'school-course');
  }
}
