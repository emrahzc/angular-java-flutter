import 'package:flutter/cupertino.dart';

class RouterIndex extends ChangeNotifier {
  int router_index = 0;

  void updateRouterIndex(int new_index) {
    router_index = new_index;
    notifyListeners();
  }
}
