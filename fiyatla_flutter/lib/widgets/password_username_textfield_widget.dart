import 'package:flutter/material.dart';

class PassUserTextField extends StatefulWidget {
  TextEditingController controller;
  String labelText;
  Function onChangedFunction;
  bool obscureText;

  PassUserTextField(
      {Key key,
      this.controller,
      this.labelText,
      this.obscureText,
      this.onChangedFunction})
      : super(key: key);

  @override
  _PassUserTextFieldState createState() => _PassUserTextFieldState();
}

class _PassUserTextFieldState extends State<PassUserTextField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
      child: TextFormField(
        controller: widget.controller,
        decoration: InputDecoration(
          border: UnderlineInputBorder(),
          labelText: widget.labelText,
          contentPadding: EdgeInsets.symmetric(vertical: 0),
        ),
        onChanged: widget.onChangedFunction,
        obscureText: widget.obscureText,
      ),
    );
  }
}
