import 'package:flutter/material.dart';

class ConditionText extends StatefulWidget {
  String txt;
  bool validity;

  ConditionText({Key key, this.txt, this.validity}) : super(key: key);

  @override
  _ConditionTextState createState() => _ConditionTextState();
}

class _ConditionTextState extends State<ConditionText> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.txt,
      style: TextStyle(color: widget.validity ? Colors.blue : Colors.red),
    );
  }
}
